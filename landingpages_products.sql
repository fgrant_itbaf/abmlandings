-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: staging.planeta.guru    Database: landingpages
-- ------------------------------------------------------
-- Server version	5.5.5-10.0.29-MariaDB-1~jessie

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `carrier` varchar(50) DEFAULT NULL,
  `title` varchar(128) DEFAULT NULL,
  `pxl_ani` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products`
--

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` VALUES (1,'juegos','all','Planeta Guru Juegos','pgj'),(2,'saberfutbol','all','Saber Fútbol','sbftbl'),(3,'copasportia','all','Copa Sportia','tyc'),(4,'saberfutbol','ar_personal','Personal Ar - Saber Fútbol','sbftbl'),(5,'saberfutbol','ar_claro','Claro Ar - Saber Fútbol','sbftbl'),(6,'saberfutbol','ar_movistar','Movistar Ar - Saber Fútbol','sbftbl'),(7,'copasportia','ar_personal','Personal Ar - Copa Sportia','tyc'),(8,'copasportia','ar_claro','Claro Ar - Copa Sportia','tyc'),(9,'copasportia','ar_movistar','Movistar Ar - Copa Sportia','tyc'),(10,'juegos','ar_personal','Personal Ar - Planeta Guru Juegos','pgj'),(11,'salute','all','Salute','slt'),(12,'juegos','co_claro','Claro Co - Planeta Guru Juegos','pgj'),(13,'juegos','auto','Superlanding - Planeta Guru Juegos','pgj'),(14,'saberfutbol','auto','Superlanding - Saber Fútbol','sbftbl'),(15,'copasportia','auto','Superlanding - Copa Sportia','tyc'),(16,'juegos','autoco','Superlanding Co - Planeta Guru Juegos','pgj'),(17,'salute','auto','Superlanding - Salute','slt'),(18,'juegos','ar_claro','Claro Ar - Planeta Guru Juegos','pgj'),(19,'salute','ar_claro','Claro Ar - Salute','slt'),(20,'juegos','ar_movistar','Movistar Ar - Planeta Guru Juegos','pgj'),(21,'juegos','mx_claro','Claro MX - Planeta Guru Juegos','pgj');
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-04-10 10:25:22
