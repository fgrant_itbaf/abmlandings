<?php 

ini_set('post_max_size', '5000M');
ini_set('upload_max_filesize', '5000M');
ini_set('memory_limit', '1280M');
date_default_timezone_set("America/Buenos_Aires");

defined('BASEPATH') OR exit('No direct script access allowed');

?><!DOCTYPE html>
<html lang="en">
<head>

	<meta charset="utf-8">

	<meta name="viewport" content="width=device-width,height=device-height,initial-scale=1,user-scalable=no">

	<title>Welcome to CodeIgniter</title>

<link rel="stylesheet" type="text/css" href="../style.css">
<script type="text/javascript" src="http://planeta.guru/cod/assets/js/scripts.js"></script>
</head>
<body onload="secciones()">

<form method="POST" action="" id="consultaLandingForm">
<input type="hidden" name="consultaLanding" id="consultaLanding" value="0" />
</form>

<form method="POST" action="" id="bajarCampanaForm">
<input type="hidden" name="bajarCampana" id="bajarCampana" value="0" />
</form>

<form method="POST" action="" id="consultaCheckForm">
<input type="hidden" name="consultaCheck" id="consultaCheck" value="0" />
<input type="hidden" name="consultaCheckName" id="consultaCheckName" value="0" />
</form>

<form method="POST" action="" id="clearForm">
<input type="hidden" name="clear" id="clear" value="" />
</form>

<div id="container">

<button onclick="lista()" value="BLista" id="BLista">Lista de Landings</button>
<button onclick="formulario()" value="BFormulario" id="BFormulario">Crear Landing</button>

<!--<button onclick="networksList()" value="formulario" id="BNetworks">Networks</button>-->

<button onclick="campanas()" value="BCampanas" id="BCampanas">Lista de Campañas</button>
<button onclick="campanaForm()" value="BCampanasForm" id="BCampanasForm">Crear Campaña</button>

<button onclick="clearForm()" value="BClear" id="BClear">Clear</button>

<br>

<?php 

/*	
	print_r($_POST);
	print_r($_FILES);
	echo "<br><br>";
*/

?><?php

if(isset($_POST['bajarCampana'])){

	$query = $this->db->query("SELECT nombre FROM campaigns WHERE nombre='".$_POST['bajarCampana']."' " );
	$nombre = $query->result();

	if(isset($nombre)){

		if(count($nombre)>0){
			$datos = Array('fecha_baja' => time(), 'activo' => '0');
			$this->db->set($datos);
			$this->db->where('nombre', $nombre[0]->nombre);
			$this->db->update('campaigns');

			$_POST = Array();
			$_FILES = Array();
		}		

	}	// metodo del boton bajar en lista de campañas

}

if(isset($_POST['clear'])){
	$_POST = Array();
	$_FILES = Array(); // metodo del boton clear 
}

if(isset($_POST['nombreCampanaInput'])){

	foreach ($_POST as $key => $value)
	{

		if($key == "nombreCampanaInput"){$dataInsert["nombre"] =  $value;}
		if($key == "descripcionCampanaInput"){$dataInsert["descripcion"] =  $value;}
		if($key == "networks"){$dataInsert["network_id"] =  $value;}
		if($key == "landingsIdCampanaInput"){$dataInsert["landings_id"] =  $value;}
		if($key == "fechaAltaInput"){$dataInsert["fecha_alta"] =  $value;}
		if($key == "fechaBajaInput"){$dataInsert["fecha_baja"] =  $value;}
		if($key == "param1CampanaInput"){$dataInsert["parametro1"] =  $value;}
		if($key == "param2CampanaInput"){$dataInsert["parametro2"] =  $value;}
		if($key == "param3CampanaInput"){$dataInsert["parametro3"] =  $value;}
		if($key == "param4CampanaInput"){$dataInsert["parametro4"] =  $value;}
		if($key == "param5CampanaInput"){$dataInsert["parametro5"] =  $value;}
		if($key == "activoCampanaInput"){$dataInsert["activo"] =  $value;}

	}

	$query = $this->db->query("SELECT fecha_alta, fecha_baja FROM campaigns WHERE nombre='".$dataInsert["nombre"]."'" );
	$fechas = $query->result();

	foreach ($fechas as $row) {

		if($row->fecha_alta == '0'){
			$dataInsert["fecha_alta"] = '0';
		}else{
			$dataInsert["fecha_alta"] = date("Y-m-d H:i:s",$row->fecha_alta);		
		}

		if($row->fecha_baja == '0'){
			$dataInsert["fecha_baja"] = '0';
		}else{
			$dataInsert["fecha_baja"] = date("Y-m-d H:i:s",$row->fecha_baja);		
		}

	}

	$query = $this->db->query("SELECT nombre FROM campaigns WHERE nombre='".$dataInsert["nombre"]."' " );
	$nombre = $query->result();

	if(isset($nombre)){

		if(count($nombre)>0){

			foreach ($nombre as $row) {
				$nombreRow = $row->nombre;
			}

			if($dataInsert['nombre'] != $nombreRow){

				$dataInsert["fecha_alta"] = time();
				$this->db->insert('campaigns', $dataInsert);
				$dataInsert = Array();

			}else{

				if(isset($_GET['h'])){//validador para diferenciar el update del editar de la creacion en 0 con mismo nombre
					$cambio = 0;
					$query = $this->db->query( " SELECT * FROM campaigns WHERE nombre='".$dataInsert["nombre"]."'" );
					$res = $query->result();

					foreach($dataInsert as $key => $value){
						if($key == "fecha_alta"){
							$dataInsert[$key] = strtotime($value);
						}
						if($key == "fecha_baja"){
							$dataInsert[$key] = strtotime($value);
						}

					}

					for($x=0;$x < count($res);$x++ ){
						foreach ($res[$x] as $key => $value) {
							if($key != "id"){
								if($value !=  $dataInsert[$key]){
									$cambio++;
								}
							}
						}
					}

					if($cambio>0){ //por fecha alta y baja que estan bloqueados

						$this->db->set($dataInsert);
						$this->db->where('nombre', $dataInsert['nombre']);
						$this->db->update('campaigns');
					}
					header('Location: index.php');
				} 

			}

		}else{

			$dataInsert["fecha_alta"] = time();
			$this->db->insert('campaigns', $dataInsert);
			$dataInsert = Array();

		}//metodo de edicion de campañas

?>
<div id="formularioCampanaForm" style="display:none;"> <!-- Formulario de edicion de campañas -->
<?php

	//form

	$this->load->helper('form');

	$attributes = array('method' => 'POST', 'id' => 'myform', 'name' => 'formCampana');
	echo form_open_multipart('http://planeta.guru/cod/index.php', $attributes);

	echo form_label('Nombre :', 'nombreCampana', $attributes);echo "<br>";

	$data = array(
		'name' => 'nombreCampanaInput',
		'placeholder' => 'Ingresar nombre',
		'class' => 'input_box',
		'style'       => 'width:50%;display:block;'
	);

	if(isset($dataInsert["nombre"])){
		$data['value'] = $dataInsert["nombre"];
	}

	echo form_input($data);

	echo form_label('Descripcion :', 'descripcionCampana', $attributes);echo "<br>";

	$data = array(
		'name' => 'descripcionCampanaInput',
		'placeholder' => 'Ingresar descripcion',
		'class' => 'input_box',
		'style'       => 'width:50%;display:block;'
	);

	if(isset($dataInsert["descripcion"])){
		$data['value'] = $dataInsert["descripcion"];
	}

	echo form_input($data);

	echo form_label('Network :', 'networkCampana', $attributes);echo "<br>";

	$options = Array();
	$queryButtonUrl = $this->db->query('SELECT nombre, id FROM networks ORDER BY nombre ASC');

	$results  = $queryButtonUrl->result();
	$options[] = "";
	foreach ($results as $row)
	{
		$options[$row->id] = $row->nombre;
	}

	if(isset($dataInsert["network_id"])){
		$data = $dataInsert["network_id"];
	}else{
		$data = '0';
	}

	echo form_dropdown('networks', $options, $data, 'id="networks" style="width: 50%" ');echo "<br>";

	echo form_label('Landings ids :', 'landingsIdCampana', $attributes);echo "<br>";

	$data = array(
		'name' => 'landingsIdCampanaInput',
		'placeholder' => 'Ingresar landings id',
		'class' => 'input_box',
		'style'       => 'width:50%;display:block;'
	);

	if(isset($dataInsert["landings_id"])){
		$data['value'] = $dataInsert["landings_id"];
	}

	echo form_input($data);

	echo form_label('Fecha alta :', 'fechaAltaCampana', $attributes);echo "<br>";

	$data = array(
		'name' => 'fechaAltaInput',
		'placeholder' => 'Ingresar fecha alta',
		'class' => 'input_box',
		'disabled' => true,
		'style'       => 'width:50%;display:block;'
	);print_r($dataInsert) . " NN ";
	if(isset($dataInsert["fecha_alta"])){
		$data['value'] = $dataInsert["fecha_alta"];
	}

	echo form_input($data);

	echo form_label('Fecha baja  :', 'fechaBajaCampana', $attributes);echo "<br>";

	$data = array(
		'name' => 'fechaBajaInput',
		'placeholder' => 'Ingresar fecha baja',
		'class' => 'input_box',
		'disabled' => true,
		'style'       => 'width:50%;display:block;'
	);

	if(isset($dataInsert["fecha_baja"])){
		$data['value'] = $dataInsert["fecha_baja"];
	}

	echo form_input($data);

	echo form_label('Param1 :', 'param1Campana', $attributes);echo "<br>";

	$data = array(
		'name' => 'param1CampanaInput',
		'placeholder' => 'Ingresar parametro 1',
		'class' => 'input_box',
		'style'       => 'width:50%;display:block;'
	);

	if(isset($dataInsert["parametro1"])){
		$data['value'] = $dataInsert["parametro1"];
	}

	echo form_input($data);

	echo form_label('Param2 :', 'param2Campana', $attributes);echo "<br>";

	$data = array(
		'name' => 'param2CampanaInput',
		'placeholder' => 'Ingresar parametro 2',
		'class' => 'input_box',
		'style'       => 'width:50%;display:block;'
	);

	if(isset($dataInsert["parametro2"])){
		$data['value'] = $dataInsert["parametro2"];
	}

	echo form_input($data);

	echo form_label('Param3 :', 'param3Campana', $attributes);echo "<br>";

	$data = array(
		'name' => 'param3CampanaInput',
		'placeholder' => 'Ingresar descripcion',
		'class' => 'input_box',
		'style'       => 'width:50%;display:block;'
	);

	if(isset($dataInsert["parametro3"])){
		$data['value'] = $dataInsert["parametro3"];
	}

	echo form_input($data);

	echo form_label('Param4 :', 'param4Campana', $attributes);echo "<br>";

	$data = array(
		'name' => 'param4CampanaInput',
		'placeholder' => 'Ingresar parametro 4',
		'class' => 'input_box',
		'style'       => 'width:50%;display:block;'
	);

	if(isset($dataInsert["parametro4"])){
		$data['value'] = $dataInsert["parametro4"];
	}

	echo form_input($data);

	echo form_label('Param5 :', 'param5Campana', $attributes);echo "<br>";

	$data = array(
		'name' => 'param5CampanaInput',
		'placeholder' => 'Ingresar parametro 5',
		'class' => 'input_box',
		'style'       => 'width:50%;display:block;'
	);

	if(isset($dataInsert["parametro5"])){
		$data['value'] = $dataInsert["parametro5"];
	}

	echo form_input($data);

	echo form_label('activo :', 'activoCampana', $attributes);

	echo '<select name="activoCampanaInput" id="activoCampanaInput" style="width:50%;display:block;" >';
	echo "<option value='' id='' ></option>";

	$array = Array (
	'0' => "No", 
	'1' => "Si"
		);

	foreach ($array as $key => $value) {
		if (!is_array($key)){
			if(trim($key) == trim($dataInsert["activo"])){
				echo "<option value='".$key."' id='".$key."' selected>".$value."</option>";
			}else{
				echo "<option value='".$key."' id='".$key."'>".$value."</option>";
			}
		}
	}

	echo '</select>';

	$data = array(
	'type' => 'submit',
	'value'=> 'Submit',
	'class'=> 'submit'

	);

	echo form_submit($data);
	echo form_close();

?>
</div>

<?php

	}else{
		echo "Insercion no concretada";
	}

}

if(isset($_POST['consultaCheck'])){

	if(isset($_POST['consultaCheckName'])){

		$query = $this->db->query("SELECT * FROM campaigns WHERE nombre='".$_POST['consultaCheckName']."'" );
		$nombre  = $query->result();

	}else{

		$query = $this->db->query("SELECT * FROM campaigns WHERE landings_id='".$_POST['consultaCheck']."'" );
		$nombre  = $query->result();

	}

	if(!isset($nombre[0]->nombre) OR isset($_POST['consultaCheckName'])){

?>
</div>
<?php
if(isset($_POST['consultaCheck'])){ ?> 
</div><div id="formularioCampanaForm2" style="display:inline;">
<?php }else{ ?>
</div><div id="formularioCampanaForm2" style="display:none;">
<?php } 

		//form de la campaña

		$this->load->helper('form');

		$attributes = array('method' => 'POST', 'id' => 'formCampana2', 'name' => 'formCampana2');
		echo form_open_multipart('http://planeta.guru/cod/index.php', $attributes);

		echo form_label('Nombre :', 'nombreCampana', $attributes);echo "<br>";

		$data = array(
			'name' => 'nombreCampanaInput',
			'placeholder' => 'Ingresar nombre',
			'class' => 'input_box',
			'style'       => 'width:50%;display:block;'
		);

		if(isset($_POST['nombreCampana'])){
			$data['value'] = $nombreCampanaInput;
		}else if(isset($nombre[0]->nombre)){
						$data['value'] = $nombre[0]->nombre;
		}

		echo form_input($data);

		echo form_label('Descripcion :', 'descripcionCampana', $attributes);echo "<br>";

		$data = array(
			'name' => 'descripcionCampanaInput',
			'placeholder' => 'Ingresar descripcion',
			'class' => 'input_box',
			'style'       => 'width:50%;display:block;'
		);

		if(isset($_POST['descripcionCapana'])){
			$data['value'] = $descripcionCampanaInput;
		}else if(isset($nombre[0]->descripcion)){
			$data['value'] = $nombre[0]->descripcion;
		}

		echo form_input($data);

		echo form_label('Network :', 'networkCampana', $attributes);echo "<br>";

		$options = Array();
		$queryButtonUrl = $this->db->query('SELECT nombre, id FROM networks ORDER BY nombre ASC');

		$results  = $queryButtonUrl->result();
		$options[] = "";
		foreach ($results as $row)
		{
			$options[$row->id] = $row->nombre;
		}

		if(isset($consultaNetwork_id)){
			$data = $consultaNetwork_id;
		}else if(isset($nombre[0]->network_id)){
			$data['value'] = $nombre[0]->network_id;
		}else{
			$data = '0';
		}

		echo form_dropdown('networks', $options, $data, 'id="networks" style="width: 50%" ');echo "<br>";

		echo form_label('Landings ids :', 'landingsIdCampana', $attributes);echo "<br>";

		$data = array(
			'name' => 'landingsIdCampanaInput',
			'placeholder' => 'Ingresar landings id',
			'class' => 'input_box',
			'style'       => 'width:50%;display:block;'
		);
		
		if(isset($_POST['consultaCheckName']) && $_POST['consultaCheckName'] == '0' ){
			$data['value'] = $_POST['consultaCheck'];
		}else if(isset($nombre[0]->landings_id)){
			$data['value'] = $nombre[0]->landings_id;
		}

		echo form_input($data);

		echo form_label('Fecha alta :', 'fechaAltaCampana', $attributes);echo "<br>";

		$data = array(
			'name' => 'fechaAltaInput',
			'placeholder' => 'Ingresar fecha alta',
			'class' => 'input_box',
						'disabled' => true,
			'style'       => 'width:50%;display:block; '
		);

		if(isset($_POST['fechaAltaInput'])){
			$data['value'] = $fechaAltaInput;
		}else if(isset($nombre[0]->fecha_alta)){


			if($nombre[0]->fecha_alta == '0'){
				$fecha_alta = '0';
			}else{
				$fecha_alta = date("Y-m-d H:i:s",$nombre[0]->fecha_alta);
			}

			$data['value'] =  $fecha_alta;

			$data['value'] =  date("Y-m-d H:i:s",$nombre[0]->fecha_alta);
		}

		echo form_input($data);

		echo form_label('Fecha baja  :', 'fechaBajaCampana', $attributes);echo "<br>";

		$data = array(
			'name' => 'fechaBajaInput',
			'placeholder' => 'Ingresar fecha baja',
			'class' => 'input_box',
						'disabled' => true,
			'style'       => 'width:50%;display:block;'
		);

		if(isset($_POST['fechaBajaInput'])){
			$data['value'] = $fechaBajaInput;
		}else if(isset($nombre[0]->fecha_baja)){

			if($nombre[0]->fecha_baja == '0'){
				$fecha_baja = '0';
			}else{
				$fecha_baja = date("Y-m-d H:i:s",$nombre[0]->fecha_baja);
			}

			$data['value'] =  $fecha_baja;

		}

		echo form_input($data);

		echo form_label('Param1 :', 'param1Campana', $attributes);echo "<br>";

		$data = array(
			'name' => 'param1CampanaInput',
			'placeholder' => 'Ingresar parametro 1',
			'class' => 'input_box',
			'style'       => 'width:50%;display:block;'
		);

		if(isset($_POST['param1CampanaInput'])){
			$data['value'] = $param1CampanaInput;
		}else if(isset($nombre[0]->parametro1)){
			$data['value'] = $nombre[0]->parametro1;
		}

		echo form_input($data);

		echo form_label('Param2 :', 'param2Campana', $attributes);echo "<br>";

		$data = array(
			'name' => 'param2CampanaInput',
			'placeholder' => 'Ingresar parametro 2',
			'class' => 'input_box',
			'style'       => 'width:50%;display:block;'
		);

		if(isset($_POST['param2CampanaInput'])){
			$data['value'] = $param2CampanaInput;
		}else if(isset($nombre[0]->parametro2)){
			$data['value'] = $nombre[0]->parametro2;
		}

		echo form_input($data);

		echo form_label('Param3 :', 'param3Campana', $attributes);echo "<br>";

		$data = array(
			'name' => 'param3CampanaInput',
			'placeholder' => 'Ingresar descripcion',
			'class' => 'input_box',
			'style'       => 'width:50%;display:block;'
		);

		if(isset($_POST['param3CampanaInput'])){
			$data['value'] = $param3CampanaInput;
		}else if(isset($nombre[0]->parametro3)){
			$data['value'] = $nombre[0]->parametro3;
		}

		echo form_input($data);

		echo form_label('Param4 :', 'param4Campana', $attributes);echo "<br>";

		$data = array(
			'name' => 'param4CampanaInput',
			'placeholder' => 'Ingresar parametro 4',
			'class' => 'input_box',
			'style'       => 'width:50%;display:block;'
		);

		if(isset($_POST['param4CampanaInput'])){
			$data['value'] = $param4CampanaInput;
		}else if(isset($nombre[0]->parametro4)){
			$data['value'] = $nombre[0]->parametro4;
		}

		echo form_input($data);

		echo form_label('Param5 :', 'param5Campana', $attributes);echo "<br>";

		$data = array(
			'name' => 'param5CampanaInput',
			'placeholder' => 'Ingresar parametro 5',
			'class' => 'input_box',
			'style'       => 'width:50%;display:block;'
		);

		if(isset($_POST['param5CampanaInput'])){
			$data['value'] = $param5CampanaInput;
		}else if(isset($nombre[0]->parametro5)){
			$data['value'] = $nombre[0]->parametro5;
		}

		echo form_input($data);

		echo form_label('activo :', 'activoCampana', $attributes);

		echo '<select name="activoCampanaInput" id="activoCampanaInput" style="width:50%;display:block;" >';
		echo "<option value='' id='' ></option>";

		$array = Array (
		'0' => "No", 
		'1' => "Si"
			);

		foreach ($array as $key => $value) {
			if (!is_array($key)){
				if(trim($key) == trim($nombre[0]->activo)){
					echo "<option value='".$key."' id='".$key."' selected>".$value."</option>";
				}else{
					echo "<option value='".$key."' id='".$key."'>".$value."</option>";
				}
			}
		}

		echo '</select>';

		$data = array(
		'type' => 'submit',
		'value'=> 'Submit',
		'class'=> 'submit'
		);

		echo form_submit($data);
		echo form_close();

		?></div><?php

	}

}

if(isset($_POST['consultaLanding'])){ //metodo para traer datos de las landings

	$query = $this->db->query('SELECT * FROM pages WHERE id='.$_POST['consultaLanding']);

	$results  = $query->result();

	foreach ($results as $row)
	{

		$consultaProduct_id = $row->product_id;
		$consultaName = $row->name;
		$consultaTitle = $row->title;
		$consultaDesc = $row->desc;
		$consultaLayout = $row->layout;
		$consultaBackground_img = $row->background_img;
		$consultaCss_file = $row->css_file;
		$consultaLogo_img = $row->logo_img;
		$consultaContent_img = $row->content_img;
		$consultaButton_img = $row->button_img;
		$consultaButton_url = $row->button_url;
		$consultaPrice = $row->price;
		$consultaTerms_url = $row->terms_url;
		$consultaBaja_keyword = $row->baja_keyword;
		$consultaNro_corto = $row->nro_corto;
		$consultaMobile_split = $row->mobile_split;
		$consultaNetwork_id = $row->network_id;
		$consultaOptimizely_id = $row->optimizely_id;
		$consultaGanalitycs_id = $row->ganalytics_id;
		$consultaActive = $row->active;
	} 
}else{

	if(isset($_POST['name'])){
		$query = $this->db->query("SELECT * FROM pages WHERE name='".$_POST['name']."'");
		$resultsUpdate = $query->result();
	}

}

?>

<div id="campanasList" style="display:none;"> <!-- Lista de Campañas -->
	lista campañas
<?php 

	$query = array();
	$query = $this->db->query('SELECT * FROM campaigns ORDER BY id DESC');

	$results = $query->result();

	//print_r($results);

	$template = array(
        'table_open'            => '<table border="1px black solid;" style="font-size:10px; padding:0px;" cellpadding="0" cellspacing="0">',
        'thead_open'            => '<thead>',
        'thead_close'           => '</thead>',
        'heading_row_start'     => '<tr>',
        'heading_row_end'       => '</tr>',
        'heading_cell_start'    => '<th>',
        'heading_cell_end'      => '</th>',
        'tbody_open'            => '<tbody>',
        'tbody_close'           => '</tbody>',
        'row_start'             => '<tr>',
        'row_end'               => '</tr>',
        'cell_start'            => '<td>',
        'cell_end'              => '</td>',
        'row_alt_start'         => '<tr>',
        'row_alt_end'           => '</tr>',
        'cell_alt_start'        => '<td>',
        'cell_alt_end'          => '</td>',
        'table_close'           => '</table>'
	);

	$this->table->set_template($template);

	$this->table->set_heading('Nombre', 'Descripcion', 'Network', 'landings_id', 'fecha_alta', 'fecha_baja'
, 'param1_value', 'param2_value', 'param3_value', 'param4_value', 'param5_value', 'activo', "acciones");

	$options = Array();
	$queryButtonUrl = $this->db->query('SELECT nombre, id FROM networks ORDER BY nombre ASC');

	$results2  = $queryButtonUrl->result();
	$networksOptions[] = "";
	foreach ($results2 as $row2)
	{
		$networksOptions[$row2->id] = $row2->nombre;
	}

	$row = Array();
	foreach ($results as $row)
	{

		//echo $row->landings_id;

		$landingsreformed = "";
		$exploded = explode("_",$row->landings_id);

		for($e=0;$e<count($exploded);$e++){
			$landingsreformed .= "<a href='/cod/index.php?landing_id=".$exploded[$e]."' target='_black' >".$exploded[$e]."</a> ";
		}

		if($row->activo == '1'){

			if($row->fecha_alta == '0'){
				$dateAlta = '0';
			}else{
				$dateAlta = date("Y-m-d H:i:s",$row->fecha_alta);
			}

			if($row->fecha_baja == '0'){
				$dateBaja = '0';
			}else{
				$dateBaja = date("Y-m-d H:i:s",$row->fecha_baja);
			}

			$this->table->add_row(
				  $row->nombre
				, $row->descripcion
				, $networksOptions[$row->network_id]
				, $landingsreformed
				, $dateAlta
				, $dateBaja
				, $row->parametro1
				, $row->parametro2
				, $row->parametro3
				, $row->parametro4
				, $row->parametro5
				, array('data' => "activo",  'style' => 'background:blue;text-align:center;') 
				, "<button id='editarCampana-".$row->nombre."' onClick=\"editarCampana('".$row->nombre."')\"> Editar </button><button id='Bajar-".$row->nombre."' onClick=\"Bajar('".$row->nombre."')\"> Bajar </button>" 
			);
		}else{

			if($row->fecha_alta == '0'){
				$dateAlta = '0';
			}else{
				$dateAlta = date("Y-m-d H:i:s",$row->fecha_alta);
			}

			if($row->fecha_baja == '0'){
				$dateBaja = '0';
			}else{
				$dateBaja = date("Y-m-d H:i:s",$row->fecha_baja);
			}

			$this->table->add_row(
				  $row->nombre
				, $row->descripcion
				, $networksOptions[$row->network_id]
				, $landingsreformed
				, $dateAlta
				, $dateBaja
				, $row->parametro1
				, $row->parametro2
				, $row->parametro3
				, $row->parametro4
				, $row->parametro5
				, array('data' => "desactivo",  'style' => 'text-align:center;') 
				, "<button id='editarCampana-".$row->nombre."' onClick=\"editarCampana('".$row->nombre."')\"> Editar </button><button id='Bajar-".$row->nombre."' onClick=\"Bajar('".$row->nombre."')\"> Bajar </button>" 
			);

		}

	}

	echo $this->table->generate();

if(isset($_POST['consultaLanding'])){ ?> 
</div><div id="lista" style="display:inline; ">
<?php }else{ ?>
</div><div id="lista" style="display:none; ">
<?php } ?>

<button onclick="crearCampana()" value="crearCampana" id="crearCampana">Crear Campaña</button>
<?php

	if(isset($_GET['landing_id'])){
		$query = $this->db->query('SELECT * FROM pages WHERE id = "'.$_GET['landing_id'].'"ORDER BY id DESC');
	}else{
		$query = $this->db->query('SELECT * FROM pages ORDER BY id DESC');
	}

	$template = array(
        'table_open'            => '<table border="1px black solid;" style="font-size:10px; padding:0px;" cellpadding="0" cellspacing="0">',
        'thead_open'            => '<thead>',
        'thead_close'           => '</thead>',
        'heading_row_start'     => '<tr>',
        'heading_row_end'       => '</tr>',
        'heading_cell_start'    => '<th>',
        'heading_cell_end'      => '</th>',
        'tbody_open'            => '<tbody>',
        'tbody_close'           => '</tbody>',
        'row_start'             => '<tr>',
        'row_end'               => '</tr>',
        'cell_start'            => '<td>',
        'cell_end'              => '</td>',
        'row_alt_start'         => '<tr>',
        'row_alt_end'           => '</tr>',
        'cell_alt_start'        => '<td>',
        'cell_alt_end'          => '</td>',
        'table_close'           => '</table>'
	);

	$this->table->set_template($template);

	$this->table->set_heading('Campañas', 'id', 'Nombre', 'Titulo', 'Descripcion', 'Network', 'Activo', 'acciones', 'Layout', 'Background_img', 'Css_File', 'Logo_img', 'Content_img', 'Button_img', 'Button_url', 'Precio', 'Términos', 'Nro Baja', 'Nro Corto', 'Mobile_Split', 'Optimizely', 'Analitycs');

	$results  = $query->result();
	//$options[] = "";

	$options = Array();
	$queryButtonUrl = $this->db->query('SELECT nombre, id FROM networks ORDER BY nombre ASC');

	$results2  = $queryButtonUrl->result();
	$networksOptions[] = "";
	foreach ($results2 as $row2)
	{
		$networksOptions[$row2->id] = $row2->nombre;
	}

	foreach ($results as $row)
	{

		// de db a link por cada img
		$row->background_img = str_replace('["',"", $row->background_img);
		$row->background_img = str_replace('"]',"", $row->background_img);
		$background_img = explode('", "', $row->background_img);

		$rr = "";
		for($x=0;$x<count($background_img);$x++){
		 $rr .= "<a href='".$background_img[$x]."' target='_blank' >".$background_img[$x]."</a><br/>";
		}
		$background_img = $rr;

		// de db a link por cada img
		$row->css_file = str_replace('["',"", $row->css_file);
		$row->css_file = str_replace('"]',"", $row->css_file);
		$css_file = explode('", "', $row->css_file);

		$rr = "";
		for($x=0;$x<count($css_file);$x++){
		 $rr .= "<a href='".$css_file[$x]."'  target='_blank' >".$css_file[$x]."</a><br/>";
		}
		$css_file = $rr;

		// de db a link por cada img
		$row->logo_img = str_replace('["',"", $row->logo_img);
		$row->logo_img = str_replace('"]',"", $row->logo_img);
		$logo_img = explode('", "', $row->logo_img);

		$rr = "";
		for($x=0;$x<count($logo_img);$x++){
		 $rr .= "<a href='".$logo_img[$x]."'  target='_blank' >".$logo_img[$x]."</a><br/>";
		}
		$logo_img = $rr;


		// de db a link por cada img
		$row->content_img = str_replace('["',"", $row->content_img);
		$row->content_img = str_replace('"]',"", $row->content_img);
		$content_img = explode('", "', $row->content_img);

		$rr = "";
		for($x=0;$x<count($content_img);$x++){
		 $rr .= "<a href='".$content_img[$x]."'  target='_blank' >".$content_img[$x]."</a><br/>";
		}
		$content_img = $rr;


		// de db a link por cada img
		$row->button_img = str_replace('["',"", $row->button_img);
		$row->button_img = str_replace('"]',"", $row->button_img);
		$button_img = explode('", "', $row->button_img);

		$rr = "";
		for($x=0;$x<count($button_img);$x++){
		 $rr .= "<a href='".$button_img[$x]."'  target='_blank' >".$button_img[$x]."</a><br/>";
		}
		$button_img = $rr;

		//$layoutParsed = "<a href='//cdn.planeta.guru/pg-landing-pages/pages/views/".$row->layout.".php'  target='_blank' >".$row->layout."</a>";

		$layoutParsed = $row->layout;

		if($row->active == "1"){
			$this->table->add_row('<input type="checkbox" name="check"  id="check-'.$row->id.'" value="'.$row->id.'">', $row->id,$row->name, $row->title, $row->desc, $networksOptions[$row->network_id], array('data' => "activo",  'style' => 'background:blue;text-align:center;') , "<button id='editar-".$row->id."' onClick=\"editar('".$row->id."')\"> Editar </button>", $layoutParsed, $background_img, $css_file, $logo_img, $content_img, $button_img, $row->button_url, $row->price, $row->terms_url, $row->baja_keyword, $row->nro_corto, $row->mobile_split, $row->optimizely_id, $row->ganalytics_id);
		}else{
			$this->table->add_row('<input type="checkbox" name="check" id="check-'.$row->id.'" value="'.$row->id.'">', $row->id, $row->name, $row->title, $row->desc, $networksOptions[$row->network_id],  array('data' => "desactivo",  'style' => 'text-align:center;') , "<button id='editar-".$row->id."' onClick=\"editar('".$row->id."')\"> Editar </button>", $layoutParsed, $background_img, $css_file, $logo_img, $content_img,$button_img, $row->button_url, $row->price, $row->terms_url, $row->baja_keyword, $row->nro_corto, $row->mobile_split, $row->optimizely_id, $row->ganalytics_id);			
		}

	}

	echo $this->table->generate();

if(isset($_POST['consultaLanding']) ){ ?>
</div><div id="formulario" style="display:inline; ">
<?php }else{ ?>
</div><div id="formulario" style="display:none; ">
<?php }

//laburar el $_POST del formulario de la landing

$dataInsert = Array();
$cambio = '0';

foreach($_POST as $key => $value){//aca poner las validaciones de cambio real que llegan para update

	if($key == "producto" && $value != ""){
		
		if(isset($resultsUpdate[0])){
			if($resultsUpdate[0]->product_id != $value){
				$cambio++;
			}	
		}

		$dataInsert['product_id'] = $value;

	}

	if($key == "name" && $value != ""){
		if(isset($resultsUpdate[0])){
			if($resultsUpdate[0]->name != $value){
				$cambio++;
			}	
		}

		$dataInsert['name'] = $value;
	}

	if($key == "title" && $value != ""){
		if(isset($resultsUpdate[0])){
			if($resultsUpdate[0]->title != $value){
				$cambio++;
			}	
		}

		$dataInsert['title'] = $value;
	}

	if($key == "desc" && $value != ""){
		if(isset($resultsUpdate[0])){
			if($resultsUpdate[0]->desc != $value){
				$cambio++;
			}
		}

		$dataInsert['desc'] = $value;
	}

	if($key == "layout" && $value != ""){ // es un php en la carpeta views!
		$exp = explode(".",$value);
		if(isset($resultsUpdate[0])){
			if($resultsUpdate[0]->layout != $exp[0]){
				$cambio++;
			}	
		}

		$dataInsert['layout'] = $exp[0];
	}

	//background_img
	if(substr($key,0,14) == "background_img" && $value != "" && !isset($dataInsert['background_img'])){
		$dataInsert['background_img'] = '["';
	}

	if(substr($key,0,14) == "background_img" && $value != "" && isset($dataInsert['background_img'])){
		$dataInsert['background_img'] .= '//cdn.planeta.guru/pg-landing-pages/pages/images/landing/fondo/'.$value;
		$dataInsert['background_img'] .= '", "';
	}

	//css file
	if(substr($key,0,8) == "css_file" && $value != "" && !isset($dataInsert['css_file'])){
		$dataInsert['css_file'] = '["';
	}

	if(substr($key,0,8) == "css_file" && $value != "" && isset($dataInsert['css_file'])){
		$dataInsert['css_file'] .= '//cdn.planeta.guru/pg-landing-pages/pages/css/'.$value;
		//$dataInsert['css_file'] .= '"]';
	}

	//logo_img
	if(substr($key,0,8) == "logo_img" && $value != "" && !isset($dataInsert['logo_img'])){
		$dataInsert['logo_img'] = '["';
	}

	if(substr($key,0,8) == "logo_img" && $value != "" && isset($dataInsert['logo_img'])){
		//echo $value . " / ";	echo $value . " & ";
		$dataInsert['logo_img'] .= '//cdn.planeta.guru/pg-landing-pages/pages/images/landing/logo/'.$value;
		$dataInsert['logo_img'] .= '", "';
	}

	//content_img
	if(substr($key,0,11) == "content_img" && $value != "" && !isset($dataInsert['content_img'])){
		$dataInsert['content_img'] = '["';
	}

	if(substr($key,0,11) == "content_img" && $value != "" && isset($dataInsert['content_img'])){

		$dataInsert['content_img'] .= '//cdn.planeta.guru/pg-landing-pages/pages/images/landing/contenido/'.$value;
		$dataInsert['content_img'] .= '", "';

	}

	//button_img
	if(substr($key,0,10) == "button_img" && $value != "" && !isset($dataInsert['button_img'])){
		$dataInsert['button_img'] = '["';
	}

	if(substr($key,0,10) == "button_img" && $value != "" && isset($dataInsert['button_img'])){
		$dataInsert['button_img'] .= '//cdn.planeta.guru/pg-landing-pages/pages/images/landing/botones/'.$value;
		$dataInsert['button_img'] .= '", "';
	}

	if($key == "button_url" && $value != ""){
		$dataInsert['button_url'] = $value;
		if(isset($resultsUpdate[0])){
			if($results[0]->button_url != $dataInsert['button_url']){
				$cambio++;
			}	
		}
	}

	if($key == "price" && $value != ""){
		$dataInsert['price'] = $value;
		if(isset($resultsUpdate[0])){
			if($results[0]->price != $dataInsert['price']){
				$cambio++;
			}	
		}
	}

	if($key == "terms_url" && $value != ""){
		$dataInsert['terms_url'] = $value;
		if(isset($resultsUpdate[0])){
			if($results[0]->terms_url != $dataInsert['terms_url']){
				$cambio++;
			}	
		}
	}

	if($key == "baja_keyword" && $value != ""){
		$dataInsert['baja_keyword'] = $value;
		if(isset($resultsUpdate[0])){
			if($results[0]->baja_keyword != $dataInsert['baja_keyword']){
				$cambio++;
			}	
		}
	}

	if($key == "nro_corto" && $value != ""){
		$dataInsert['nro_corto'] = $value;
		if(isset($resultsUpdate[0])){
			if($results[0]->nro_corto != $dataInsert['nro_corto']){
				$cambio++;
			}	
		}
	}

	if($key == "mobile_split" && $value != ""){
		$dataInsert['mobile_split'] = $value;
		if(isset($resultsUpdate[0])){
			if($results[0]->mobile_split != $dataInsert['mobile_split']){
				$cambio++;
			}	
		}
	}

	if($key == "networks" && $value != ""){
		$dataInsert['network_id'] = $value;
		if(isset($resultsUpdate[0])){
			if($results[0]->network_id != $dataInsert['network_id']){
				$cambio++;
			}
		}
	}

	if($key == "optimizely_id" && $value != ""){
		$dataInsert['optimizely_id'] = $value;
		if(isset($resultsUpdate[0])){
			if($results[0]->optimizely_id != $dataInsert['optimizely_id']){
				$cambio++;
			}
		}
	}

	if($key == "ganalytics_id" && $value != ""){
		$dataInsert['ganalytics_id'] = $value;

		if(isset($resultsUpdate[0])){
			if($results[0]->ganalytics_id != $dataInsert['ganalytics_id']){
				$cambio++;
			}
		}

	}

	if($key == "timestamp" && $value != ""){
		$dataInsert['timestamp'] = time();

		if(isset($resultsUpdate[0])){
			if($results[0]->timestamp != $dataInsert['timestamp']){
				//$cambio++;
			}
		}

	}

	if($key == "active" && $value != ""){
		$dataInsert['active'] = $value;

		if(isset($resultsUpdate[0])){
			if($results[0]->active != $dataInsert['active']){
				$cambio++;
			}
		}

	}

}

foreach($_FILES as $key => $value){

	//layout
	if(substr($key,0,6) == "layout"  && $value['name'] != "" && !isset($dataInsert['layout'])){
		$dataInsert['layout'] = $value['name'];

		$dir_subida = "../pg-landing-pages/pages/views/".$value['name'];
		move_uploaded_file($_FILES[$key]['tmp_name'], $dir_subida);
	}

	//css_file
	if(substr($key,0,8) == "css_file"  && $value['name'] != "" && !isset($dataInsert['css_file'])){
		$dataInsert['css_file'] = '["';
	}

	if(substr($key,0,8) == "css_file" && $value['name'] != "" && isset($dataInsert['css_file'])){

		$dataInsert['css_file'] .= '//cdn.planeta.guru/pg-landing-pages/pages/css/'.$value['name'];
		$dataInsert['css_file'] .= '", "';

		$dir_subida = "../pg-landing-pages/pages/css/".$value['name'];
		move_uploaded_file($_FILES[$key]['tmp_name'], $dir_subida);

	}

	//content_img
	if(substr($key,0,11) == "content_img" && $value['name'] != "" && !isset($dataInsert['content_img'])){
		$dataInsert['content_img'] = '["';
	}

	if(substr($key,0,11) == "content_img" && $value['name'] != "" && isset($dataInsert['content_img'])){

		$dataInsert['content_img'] .= '//cdn.planeta.guru/pg-landing-pages/pages/images/landing/contenido/'.$value['name'];
		$dataInsert['content_img'] .= '", "';

		$dir_subida = "../pg-landing-pages/pages/images/landing/contenido/".$value['name'];
		move_uploaded_file($_FILES[$key]['tmp_name'], $dir_subida);

	}

	//button_img
	if(substr($key,0,10) == "button_img" && $value['name'] != "" && !isset($dataInsert['button_img'])){
		$dataInsert['button_img'] = '["';
	}

	if(substr($key,0,10) == "button_img" && $value['name'] != "" && isset($dataInsert['button_img'])){

		$dataInsert['button_img'] .= '//cdn.planeta.guru/pg-landing-pages/pages/images/landing/botones/'.$value['name'];
		$dataInsert['button_img'] .= '", "';

		$dir_subida = "../pg-landing-pages/pages/images/landing/botones/".$value['name'];
		move_uploaded_file($_FILES[$key]['tmp_name'], $dir_subida);

	}

	//logo_img
	if(substr($key,0,8) == "logo_img" && $value['name'] != "" && !isset($dataInsert['logo_img'])){
		$dataInsert['logo_img'] = '["';
	}

	if(substr($key,0,8) == "logo_img" && $value['name'] != "" && isset($dataInsert['logo_img'])){

		$dataInsert['logo_img'] .= '//cdn.planeta.guru/pg-landing-pages/pages/images/landing/logo/'.$value['name'];
		$dataInsert['logo_img'] .= '", "';

		$dir_subida = "../pg-landing-pages/pages/images/landing/logo/".$value['name'];
		move_uploaded_file($_FILES[$key]['tmp_name'], $dir_subida);

	}

	//background_img
	if(substr($key,0,14) == "background_img"  && $value['name'] != "" && !isset($dataInsert['background_img'])){
		$dataInsert['background_img'] = '["';
	}

	if(substr($key,0,14) == "background_img" && $value['name'] != "" && isset($dataInsert['background_img'])){

		$dataInsert['background_img'] .= '//cdn.planeta.guru/pg-landing-pages/pages/images/landing/fondo/'.$value['name'];
		$dataInsert['background_img'] .= '", "';

		$dir_subida = "../pg-landing-pages/pages/images/landing/fondo/".$value['name'];
		move_uploaded_file($_FILES[$key]['tmp_name'], $dir_subida);
	}

}

//arreglos para la insercion
if(isset($dataInsert['background_img'])){

	$dataInsert['background_img'] = rtrim($dataInsert['background_img'], '", "');
	$dataInsert['background_img'] .= '"]';

	if(isset($resultsUpdate[0])){
		if($resultsUpdate[0]->background_img != $dataInsert['background_img']){
			$cambio++;
		}
	}

}

if(isset($dataInsert['content_img'])){

	$dataInsert['content_img'] = rtrim($dataInsert['content_img'], '", "');

	//aca la verificacion para que agregue o no segun update;

	$dataInsert['content_img'] .= '"]';
	//echo $dataInsert['content_img'];

	if(isset($resultsUpdate[0])){
		if($resultsUpdate[0]->content_img != $dataInsert['content_img']){
			$cambio++;
		}
	}

}

if(isset($dataInsert['button_img'])){

	$dataInsert['button_img'] = rtrim($dataInsert['button_img'], '", "');
	$dataInsert['button_img'] .= '"]';

	if(isset($resultsUpdate[0])){
		if($resultsUpdate[0]->button_img != $dataInsert['button_img']){
			$cambio++;
		}
	}

}

if(isset($dataInsert['css_file'])){

	$dataInsert['css_file'] = rtrim($dataInsert['css_file'], '", "');
	$dataInsert['css_file'] .= '"]';

	if(isset($resultsUpdate[0])){
		if($resultsUpdate[0]->css_file != $dataInsert['css_file']){
			$cambio++;
		}
	}

}

if(isset($dataInsert['logo_img'])){
	$dataInsert['logo_img'] = rtrim($dataInsert['logo_img'], '", "');
	$dataInsert['logo_img'] .= '"]';
	
	if(isset($resultsUpdate[0])){
		if($resultsUpdate[0]->logo_img != $dataInsert['logo_img']){
			$cambio++;
		}
	}

}

//insercion
if(isset($results) && isset($_POST)){

	if(isset($_POST['duplicacion'])){
		$query = $this->db->query("SELECT name FROM pages WHERE name='".$_POST['name']."'");
		$name  = $query->result();
		if(isset($name[0]->name)){
			$this->db->insert('pages', $dataInsert);
		}

	} else {

		if($cambio>'0' && count($_POST)>0) {//tengo que poner una validacion de cambio real
			$query = $this->db->query("SELECT name FROM pages WHERE name='".$_POST['name']."'");
			$name  = $query->result();


			if (isset($name[0]->name)){
				
				//echo "UPDATE: <BR>";
				$this->db->set($dataInsert);
				$this->db->where('name', $name[0]->name);
				$this->db->update('pages');
			}

		}else{
			//echo "INSERT: <BR>";
			//print_r($dataInsert);
			if(isset($dataInsert['name'])){
				$query = $this->db->query("SELECT name FROM pages WHERE name='".$_POST['name']."'");
				$name  = $query->result();
				if(!isset($name[0]->name)){
					if(count($dataInsert)>3){
						$this->db->insert('pages', $dataInsert);
						$dataInsert = Array();
					}
				}				
			}

		}

	}

}

//form

$this->load->helper('form');

$attributes = array('method' => 'POST', 'id' => 'myform');
echo form_open_multipart('http://planeta.guru/cod/index.php', $attributes);

/*  select de duplicacion */
?><div style="border:1px black solid"><?php
/*  select de productos */

?></div><div style="border:1px black solid"><?php

$attributes = array('style' => 'width:50%;display:block;');
echo form_label('Producto :', 'producto',$attributes);

$query = $this->db->query('SELECT * FROM products ORDER BY name ASC');

$results  = $query->result();

echo "<div id='productoUpload'>";

echo "</div><div id='productoSelect'>";

echo '<select name="producto" id="productoS" style="width:50%;display:block;" >';

if(!isset($consultaProduct_id)){

	echo "<option value='' id='' selected></option>";
	foreach ($results as $row)
	{
		echo "<option value='".$row->id."' id='".$row->id."'>".$row->name."-".$row->carrier."</option>";
	}

}else{

	echo "<option value='' id='' selected></option>";
	foreach ($results as $row)
	{
		if($row->id == $consultaProduct_id){
			echo "<option value='".$row->id."' id='".$row->id."' selected>".$row->name."-".$row->carrier."</option>";
		}else{
			echo "<option value='".$row->id."' id='".$row->id."'>".$row->name."-".$row->carrier."</option>";
		}
	}

}
echo '</select>';

?></div></div><div style="border:1px black solid"><?php
/*  input de name */

$attributes = array(
	'style' => 'width:50%;display:block;'
);

echo form_label('Name  :', 'name',$attributes);

$data = array(
	'name' => 'name',
	'placeholder' => 'Ingresar nombre',
	'class' => 'input_box',
	'style'       => 'width:50%;display:block;'
);

if(isset($_POST['consultaLanding'])){
	$data['value'] = $consultaName;
}

echo form_input($data);
?></div><div style="border:1px black solid"><?php
/*  input de title */
echo form_label('Title :', 'title',$attributes);

$data = array(
'name' => 'title',
'placeholder' => 'Ingresar titulo',
'class' => 'input_box',
'style'       => 'width:50%;display:block;'
	);


if(isset($_POST['consultaLanding'])){
	$data['value'] = $consultaTitle;
}

echo form_input($data);
?></div><div style="border:1px black solid"><?php
/*  input de desc */
echo form_label('Desc :', 'desc',$attributes);

$data = array(
	'name' => 'desc',
	'placeholder' => 'Ingresar descripcion',
	'class' => 'input_box',
	'style'       => 'width:50%;display:block;'
);

if(isset($_POST['consultaLanding'])){
	$data['value'] = $consultaDesc;
}

echo form_input($data);
?></div><div style="border:1px black solid"><?php
/*  select de layout */

echo form_label('Layout :', 'layout', $attributes);

echo "<div id='layoutS'>
<input type='button' id='layoutFButtonS' name='layoutFButtonS' value='S+' onclick='layoutSelectB()' />";
$map = directory_map('../pg-landing-pages/pages/views/');

echo '<select name="layout" id="layoutSelect" style="width:50%;display:block;" >';
echo "<option value='' id=''></option>";

foreach ($map as $key => $value) {
	if (!is_array($value)){

		if(isset($consultaLayout)){
	
			$e = explode(".inc.php",$value);
			if (count($e)>1){$value = $e[0];}
			$ee = explode(".inc.php",$consultaLayout);
			if (count($e)>1){$consultaLayout = $ee[0];}

			if(trim($value) == trim($consultaLayout)){
				echo "<option value='".$value."' id='".$value."' selected>".$value."</option>";
			}else{
				echo "<option value='".$value."' id='".$value."'>".$value."</option>";
			}

		} else {
				echo "<option value='".$value."' id='".$value."' selected>".$value."</option>";
		}

	}
}

echo '</select>';

echo "</div><div id='layoutF'> <input type='button' id='layoutFButtonF' name='layoutFButtonF' value='F+' onclick='layoutUploadB()' />";

$upload = Array ("name" => "layoutFile-0", "Class" => "Upload", "id" => "layoutUpload");
echo form_upload ($upload);
?></div>
<?php
/*  select de background_img */

echo '<div id="background_imgDiv" style="border:1px black solid">';
echo form_label('background_img :', 'background_img', $attributes);

echo "<div id='background_imgS'> ";
//<input type='button' id='background_imgButtonS' name='background_imgButtonS' value='F+' onclick='background_imgS()' />
$map = directory_map('../pg-landing-pages/pages/images/landing/fondo');

$data = array('name'    => 'background_imgAgregar',
              'id'      => 'background_imgAgregar',
              'value'   => 'TRUE',
              'type'    => 'button',
              'content' => '+',
              'onClick' => 'agregarBackground_imgFile()'
);

echo form_button($data); 

$data = array('name'    => 'background_imgRestar',
              'id'      => 'background_imgRestar',
              'value'   => 'TRUE',
              'type'    => 'button',
              'content' => '-',
              'onClick' => 'restarBackground_imgFile()'
);

echo form_button($data);

if(!isset($consultaBackground_img)){

	echo '<select name="background_img-0" id="background_img-0" style="width:50%;display:block;" >';
	echo "<option value='' id='' selected></option>";
	foreach ($map as $key => $value) {
		if (!is_array($value)){
			echo "<option value='".$value."' id='".$value."'>".$value."</option>";
		}
	}

	echo '</select>';

}else{

	$exploded = Array();
	$exploded = explode('", "',$consultaBackground_img);

	for($x=0;$x<count($exploded);$x++){
		$exploded[$x] = str_replace('["', "", $exploded[$x]);

		echo '<select name="background_img-'.$x.'" id="background_img-'.$x.'" style="width:50%;display:block;" >';
		echo "<option value='' id='' ></option>";

		foreach ($map as $key => $value) {
			if (!is_array($value)){
				$exploded2 = explode("/", $exploded[$x]);
				$exploded2 = array_reverse($exploded2);
				$exploded2 = str_replace('"]', "", $exploded2[0]);

				if(trim($value) == trim($exploded2)){
					echo "<option value='".$value."' id='".$value."' selected>".$value."</option>";
				}else{
					echo "<option value='".$value."' id='".$value."'>".$value."</option>";
				}

			}
		}

		echo '</select>';

	}

}

echo "</div><div id='background_imgF'> ";

// <input type='button' id='background_imgButtonF' name='background_imgButtonF' value='S+' onclick='background_imgF()' />
$data = array('name'    => 'background_imgUploadAgregar',
              'id'      => 'background_imgUploadAgregar',
              'value'   => 'TRUE',
              'type'    => 'button',
              'content' => '+',
              'onClick' => 'agregarBackground_imgFileUpload()'
);

echo form_button($data); 

$data = array('name'    => 'background_imgUploadRestar',
              'id'      => 'background_imgUploadRestar',
              'value'   => 'TRUE',
              'type'    => 'button',
              'content' => '-',
              'onClick' => 'restarBackground_imgFileUpload()'
);

echo form_button($data);

$upload = Array ("name" => "background_imgFileUpload-0", "Class" => "Upload", "id" => "background_imgFileUpload-0");
echo form_upload ($upload);

?></div><div id="css_fileDiv" style="border:1px black solid"><?php
/*  select de css_file */

echo form_label('css_file :', 'css_file', $attributes);
echo "<div id='cssSelectDiv'>  <input type='button' id='cssSelectButton' name='cssSelectButton' value='S+' onclick='cssSelectB()' />";
$map = directory_map('../pg-landing-pages/pages/css');

echo '<select name="css_file" id="css_fileSelect" style="width:50%;display:block;" >';
echo "<option value='' id='' selected></option>";

if(isset($consultaCss_file)){$e = explode("cdn",$consultaCss_file);}
if(isset($e[1])){$e = explode("\"",$e[1]);}
if(isset($e[0])){$consultaCss_fileExploded = explode("css/",$e[0]);}

if(!isset($consultaCss_fileExploded[1])){
	echo "<option value='' id='' selected></option>";
	foreach ($map as $key => $value) {
		if (!is_array($value)){
			echo "<option value='".$value."' id='".$value."'>".$value."</option>";
		}
	}
}else{

	foreach ($map as $key => $value) {
		if (!is_array($value)){
			if(isset($consultaCss_fileExploded[1])){
				if(trim($value) == trim($consultaCss_fileExploded[1])){
					echo "<option value='".$value."' id='".$value."' selected>".$value."</option>";
				}else{
					echo "<option value='".$value."' id='".$value."'>".$value."</option>";
				}
			}
		}
	}

}

echo '</select>';

echo "</div><div id='cssUploadDiv'>  <input type='button' id='cssUploadButton' name='cssUploadButton' value='F+' onclick='cssUploadB()' />";
$upload = Array ("name" => "css_fileFile-0", "Class" => "Upload", "name" => "css_fileUpload", "id" => "css_fileUpload");
echo form_upload ($upload);
?></div><div id="logo_imgDiv" style="border:1px black solid"><?php
/*  select de logo_img */

echo form_label('logo_img :', 'logo_img', $attributes);

echo "<div id='logo_imgSelectDiv'>  <input type='button' id='logo_imgSelectButton' name='logo_imgSelectButton' value='S+' onclick='logo_imgSelectB()' />";
$map = directory_map('../pg-landing-pages/pages/images/landing/logo');

//solo uno

echo '<select name="logo_img" id="logo_img-0" style="width:50%;display:block;" >';
echo "<option value='' id='' selected></option>";

if(!isset($consultaLogo_img)){

	foreach ($map as $key => $value) {
		if (!is_array($value)){
			echo "<option value='".$value."' id='".$value."'>".$value."</option>";
		}
	}

}else{
		
	$e = explode("ar_personal/solapas/",$consultaLogo_img);
	if(count($e)>1){
		$consultaLogo_img = str_replace('"]', '',$e[1]);
	}
	$e = explode("ar_personal/header/",$consultaLogo_img);
	if(count($e)>1){
		$consultaLogo_img = str_replace('"]', '',$e[1]);
	}
	$e = explode("images/landing/",$consultaLogo_img);
	if(count($e)>1){
		$consultaLogo_img = str_replace('"]', '',$e[1]);
	}
	
	$e = explode("logo/",$consultaLogo_img);
	$consultaLogo_img = $e[1];

	foreach ($map as $key => $value) {
		if (!is_array($value)){

			if(trim($value) == trim($consultaLogo_img)){
				echo "<option value='".$value."' id='".$value."' selected>".$value."</option>";
			}else{
				echo "<option value='".$value."' id='".$value."'>".$value."</option>";
			}

		}
	}

}

echo '</select>';

echo "</div><div id='logo_imgUploadDiv'>  <input type='button' id='logo_imgUploadButton' name='logo_imgUploadButton' value='F+' onclick='logo_imgUploadB()' />";
$upload = Array ("name" => "logo_imgFile-0", "id" => "logo_imgFile-0", "Class" => "Upload");
echo form_upload ($upload);
?></div><div id="content_imgDiv" style="border:1px black solid"><?php
/*  select de content_img */

echo form_label('content_img :', 'content_img', $attributes);

$map = directory_map('../pg-landing-pages/pages/images/landing/contenido');

$data = array('name'    => 'content_imgFileAgregar',
              'id'      => 'content_imgFileAgregar',
              'value'   => 'TRUE',
              'type'    => 'button',
              'content' => '+',
              'onClick' => 'agregarContent_imgFile()'
);

echo form_button($data); 

$data = array('name'    => 'content_imgFileRestar',
              'id'      => 'content_imgFileRestar',
              'value'   => 'TRUE',
              'type'    => 'button',
              'content' => '-',
              'onClick' => 'restarContent_imgFile()'
);

echo form_button($data);

if(!isset($consultaContent_img)){

	echo '<select name="content_img-0" id="content_imgFile-0" style="width:50%;display:block;" >';
	echo "<option value='' id='' selected></option>";
	foreach ($map as $key => $value) {
		if (!is_array($value)){
			echo "<option value='".$value."' id='".$value."'>".$value."</option>";
		}
	}
	echo '</select>';

}else{

	$exploded = Array();
	$exploded = explode('", "',$consultaContent_img);

	for($x=0;$x<count($exploded);$x++){

		echo '<select name="content_img-'.$x.'" id="content_imgFile-'.$x.'" style="width:50%;display:block;" >';

		$e = explode("//",$exploded[$x]);
		$e = explode("\"]",$e[1]);
		$e = explode("landing/",$e[0]);
		$e = str_replace('", "', "",$e[1]);
		$e = str_replace('contenido/', "",$e);

		foreach ($map as $key => $value) {
			if (!is_array($value)){
				if(trim($value) == trim($e)){
					echo "<option value='".$value."' id='".$value."' selected>".$value."</option>";
				}else{
					echo "<option value='".$value."' id='".$value."'>".$value."</option>";
				}
			}
		}

		echo '</select>';

	}

}

$data = array('name'    => 'content_imgUploadAgregar',
              'id'      => 'content_imgUploadAgregar',
              'value'   => 'TRUE',
              'type'    => 'button',
              'content' => '+',
              'onClick' => 'agregarContent_imgFileUpload()'
);

echo form_button($data); 

$data = array('name'    => 'content_imgUploadRestar',
              'id'      => 'content_imgUploadRestar',
              'value'   => 'TRUE',
              'type'    => 'button',
              'content' => '-',
              'onClick' => 'restarContent_imgFileUpload()'
);

echo form_button($data);

$upload = Array ("name" => "content_imgFileUpload-0", "Class" => "Upload", 'id' => 'content_imgFileUpload-0');
echo form_upload ($upload);

?></div><div id="button_imgDiv" style="border:1px black solid"><?php
/*  select de button_img */

echo form_label('button_img :', 'button_img', $attributes);

$map = directory_map('../pg-landing-pages/pages/images/landing/botones');

$data = array('name'    => 'button_imgFileAgregar',
              'id'      => 'button_imgFileAgregar',
              'value'   => 'TRUE',
              'type'    => 'button',
              'content' => '+',
              'onClick' => 'agregarButton_imgFile()'
);

echo form_button($data); 
$data = array('name'    => 'button_imgFileRestar',
              'id'      => 'button_imgFileRestar',
              'value'   => 'TRUE',
              'type'    => 'button',
              'content' => '-',
              'onClick' => 'restarButton_imgFile()'
);

echo form_button($data);

if(!isset($consultaButton_img)){

	echo '<select name="button_img-0" id="button_imgFile-0" style="width:50%;display:block;" >';
	echo "<option value='' id='' selected></option>";
	foreach ($map as $key => $value) {
		if (!is_array($value)){
			echo "<option value='".$value."' id='".$value."'>".$value."</option>";
		}
	}
	echo '</select>';

}else{

	$exploded = explode('", "',$consultaButton_img);

	if(count($exploded)>1){
		$validOne = FALSE;
	}else{
		$exploded = Array();
		$e = explode("/",$consultaButton_img);
		$e = array_reverse($e);
		$exploded[] = str_replace('"]', "", $e[0]);
		$validOne = TRUE;
	}

	for($x=0;$x<count($exploded);$x++){

		if(!$validOne){
			$exploded[$x] = str_replace('["', "", $exploded[$x]);
			$e = explode("landing/botones/",$exploded[$x]);
			$e = explode("\"]",$e[1]);
			$exploded[$x] = $e[0];
		}

		echo '<select name="button_img-'.$x.'" id="button_imgFile-'.$x.'" style="width:50%;display:block;" >';
		echo "<option value='' id='' selected></option>";

		foreach ($map as $key => $value) {

			if (!is_array($value)){
				if(trim($value) == trim($exploded[$x])){
					echo "<option value='".$value."' id='".$value."' selected>".$value."</option>";
				}else{
					echo "<option value='".$value."' id='".$value."'>".$value."</option>";
				}
			}
		}

		echo '</select>';

	}

}

$data = array('name'    => 'button_imgAgregarUpload',
              'id'      => 'button_imgAgregarUpload',
              'value'   => 'TRUE',
              'type'    => 'button',
              'content' => '+',
              'onClick' => 'agregarButton_imgFileUpload()'
);

echo form_button($data); 

$data = array('name'    => 'button_imgRestarUpload',
              'id'      => 'button_imgRestarUpload',
              'value'   => 'TRUE',
              'type'    => 'button',
              'content' => '-',
              'onClick' => 'restarButton_imgFileUpload()'
);

echo form_button($data);

$upload = Array ("name" => "button_imgFileUpload-0", "Class" => "Upload");
echo form_upload ($upload);
?></div><div style="border:1px black solid"><?php
/*  select de button_url */

	$options = Array();
	$queryButtonUrl = $this->db->query('SELECT * FROM urls WHERE url !="" ');

	$results  = $queryButtonUrl->result();
	$options[] = "";
	foreach ($results as $row)
	{
		$options[$row->button_url_code] = $row->url;
	}

echo form_label('button_url :', 'button_url', $attributes);

$data = array(
'name' => 'button_url',
'placeholder' => 'Ingresar button url',
'class' => 'input_box',
'style'       => 'width:50%;display:block;'
	);

if(isset($consultaButton_url)){
	$data['value'] = $consultaButton_url;
}

echo form_input($data);

?></div><div style="border:1px black solid"><?php
/*  select de price */

echo form_label('price :', 'price', $attributes);

$data = array(
'name' => 'price',
'placeholder' => 'Ingresar precio',
'class' => 'input_box',
'style'       => 'width:50%;display:block;'
	);

if(isset($_POST['consultaLanding'])){
	$data['value'] = $consultaPrice;
}

echo form_input($data);
?></div><div style="border:1px black solid"><?php
/*  select de terms_url */

echo form_label('terms_url :', 'terms_url', $attributes);

$data = array(
'name' => 'terms_url',
'placeholder' => 'Ingresar terms url',
'class' => 'input_box',
'style'       => 'width:50%;display:block;'
	);

if(isset($_POST['consultaLanding'])){
	$data['value'] = $consultaTerms_url;
}

echo form_input($data);

?></div><div style="border:1px black solid"><?php
/*  select de baja_keyword */

echo form_label('baja_keyword :', 'baja_keyword', $attributes);

$data = array(
'name' => 'baja_keyword',
'placeholder' => 'Ingresar baja_keyword',
'class' => 'input_box',
'style'       => 'width:50%;display:block;'
	);

if(isset($_POST['consultaLanding'])){
	$data['value'] = $consultaBaja_keyword;
}

echo form_input($data);

?></div><div style="border:1px black solid"><?php
/*  select de nro_corto */

echo form_label('nro_corto :', 'nro_corto', $attributes);

$data = array(
'name' => 'nro_corto',
'placeholder' => 'Ingresar nro_corto',
'class' => 'input_box',
'style'       => 'width:50%;display:block;'
	);

if(isset($_POST['consultaLanding'])){
	$data['value'] = $consultaNro_corto;
}

echo form_input($data);

?></div><div style="border:1px black solid"><?php
/*  select de mobile_split */

echo form_label('mobile_split :', 'mobile_split', $attributes);

$data = array(
'name' => 'mobile_split',
'placeholder' => 'Ingresar mobile_split',
'class' => 'input_box',
'style'       => 'width:50%;display:block;'
	);

if(isset($_POST['consultaLanding'])){
	$data['value'] = $consultaMobile_split;
}

echo form_input($data);

?></div><div style="border:1px black solid"><?php
/*  select de networks */

	$options = Array();
	$queryButtonUrl = $this->db->query('SELECT nombre, id FROM networks ORDER BY nombre ASC');

	$results  = $queryButtonUrl->result();
	$options[] = "";
	foreach ($results as $row)
	{
		$options[$row->id] = $row->nombre;
	}

echo form_label('networks :', 'nombre', $attributes);

if(isset($consultaNetwork_id)){
	$data = $consultaNetwork_id;
}else{
	$data = '0';
}

echo form_dropdown('networks', $options, $data, 'id="networks" style="width: 50%" ');

?></div><div style="border:1px black solid"><?php
/*  select de optimizely_id */

echo form_label('optimizely_id :', 'optimizely_id', $attributes);

$data = array(
'name' => 'optimizely_id',
'placeholder' => 'Ingresar optimizely_id',
'class' => 'input_box',
'style'       => 'width:50%;display:block;'
	);

if(isset($_POST['consultaLanding'])){
	$data['value'] = $consultaOptimizely_id;
}

echo form_input($data);

?></div><div style="border:1px black solid"><?php
/*  select de ganalytics_id */

echo form_label('ganalytics_id :', 'ganalytics_id', $attributes);

$data = array(
'name' => 'ganalytics_id',
'placeholder' => 'Ingresar ganalytics_id',
'class' => 'input_box',
'style'       => 'width:50%;display:block;'
	);

if(isset($_POST['consultaLanding'])){
	$data['value'] = $consultaGanalitycs_id;
}

echo form_input($data);

echo form_label('activo :', 'activo', $attributes);

echo '<select name="active" id="active" style="width:50%;display:block;" >';
echo "<option value='' id='' ></option>";

$array = Array (
'0' => "No", 
'1' => "Si"
	);

foreach ($array as $key => $value) {

	if (!is_array($key)){
		if(trim($key) == trim($consultaActive)){
			echo "<option value='".$key."' id='".$key."' selected>".$value."</option>";
		}else{
			echo "<option value='".$key."' id='".$key."'>".$value."</option>";
		}
	}
}

echo '</select>';

?></div><?php
/* submit button*/
$data = array(
'type' => 'submit',
'value'=> 'Submit',
'class'=> 'submit'
);

echo form_submit($data);
echo form_close();

?></div>
</div>

<script type="text/javascript">

var productoFile = 0;
var productoFileFor = false;
var layoutFile = 0;
var layoutFileFor = false;
var background_imgFile = 0;
var background_imgFileFor = false;
var background_imgFileUpload = 0;
var background_imgFileUploadFor = false;
var Css_fileFile = 0;
var Css_fileFileFor = false;
var logo_imgFile = 0;
var logo_imgFileFor = false;
var content_imgFile = 0;
var content_imgFileFor = false;
var content_imgFileUpload = 0;
var content_imgFileUploadFor = false;
var button_imgFile = 0;
var button_imgFileFor = false;
var button_imgFileUpload = 0;
var button_imgFileUploadFor = false;

function agregarBackground_imgFile (){

	if(!background_imgFileFor){
	var list = document.getElementsByTagName("select");

		for(var c=0;c < list.length;c++){
			if(list[c].name.substr(0,14) == "background_img"){
				background_imgFile++;
			}
			background_imgFileFor = true;
		}

	}

	var background_imgInnerHTML = document.getElementById("background_img-0").innerHTML;
	background_imgFile++;

	background_imgInnerHTML = '<select name="background_img-'+background_imgFile+'"  id="background_img-'+background_imgFile+'" style="width: 50%">'+background_imgInnerHTML+'</select>';

	var background_imgSDiv = document.getElementById("background_imgS");
	background_imgSDiv.innerHTML = background_imgSDiv.innerHTML + background_imgInnerHTML;

}

function restarBackground_imgFile (){

	if(!background_imgFileFor){
	var list = document.getElementsByTagName("select");

		for(var c=0;c < list.length;c++){
			if(list[c].name.substr(0,14) == "background_img"){
				background_imgFile++;
			}
			background_imgFileFor = true;
		}
		background_imgFile--;
	}

	if(background_imgFile > '0'){
		var background_imgSDiv = document.getElementById("background_imgS");
		var background_imgInnerHTML = document.getElementById("background_img-"+background_imgFile);
		background_imgSDiv.removeChild(background_imgInnerHTML);
		background_imgFile--;
	}
}

function agregarBackground_imgFileUpload (){

	if(!background_imgFileUploadFor){
		var list = document.getElementsByTagName("input");
		for(var c=0;c < list.length;c++){
			if(list[c].name.substr(0,24) == "background_imgFileUpload"){
				background_imgFileUpload++;
			}
			background_imgFileUploadFor = true;
		}
	}

	background_imgFileUploadInnerHTML = "<input name='background_imgFileUpload-"+background_imgFileUpload+"' id='background_imgFileUpload-"+background_imgFileUpload+"' class='Upload' type='file'>";

	background_imgFileUpload++;

	var background_imgDiv = document.getElementById("background_imgF");

	background_imgDiv.innerHTML = background_imgDiv.innerHTML + background_imgFileUploadInnerHTML;

}

function restarBackground_imgFileUpload (){

	if(!background_imgFileUploadFor){
		var list = document.getElementsByTagName("input");
		for(var c=0;c < list.length;c++){
			if(list[c].name.substr(0,24) == "background_imgFileUpload"){
				background_imgFileUpload++;
			}
			background_imgFileUploadFor = true;
		}
		background_imgFileUpload--;
	}

	alert(background_imgFileUpload);

	if(background_imgFileUpload > '0'){
		var background_imgFDiv = document.getElementById("background_imgF");
		var background_imgInnerHTML = document.getElementById("background_imgFileUpload-"+background_imgFileUpload);
		background_imgFDiv.removeChild(background_imgInnerHTML);
		background_imgFileUpload--;
	}
}

function agregarContent_imgFile (){

	if(!content_imgFileFor){
		var list = document.getElementsByTagName("select");

		for(var c=0;c < list.length;c++){
			if(list[c].name.substr(0,11) == "content_img"){
				content_imgFile++;
			}
			content_imgFileFor = true;
		}
		content_imgFile--;
	}
	
	var content_imgFileInnerHTML = document.getElementById("content_imgFile-0").innerHTML;
	content_imgFile++;

	content_imgFileInnerHTML = '<select name="content_imgFile-'+content_imgFile+'"  id="content_imgFile-'+content_imgFile+'" style="width: 50%">'+content_imgFileInnerHTML+'</select>';

	var content_imgDiv = document.getElementById("content_imgDiv");
	content_imgDiv.innerHTML = content_imgDiv.innerHTML + content_imgFileInnerHTML;

}

function restarContent_imgFile (){

	if(!content_imgFileFor){
		var list = document.getElementsByTagName("select");

		for(var c=0;c < list.length;c++){
			if(list[c].name.substr(0,11) == "content_img"){
				content_imgFile++;
			}
			content_imgFileFor = true;
		}
		content_imgFile--;
	}

	if(content_imgFile > '0'){

		var content_imgInnerHTML = document.getElementById("content_imgFile-"+content_imgFile);
		content_imgDiv.removeChild(content_imgInnerHTML);
		content_imgFile--;
	}

}

function agregarContent_imgFileUpload (){

	if(!content_imgFileUploadFor){
	var list = document.getElementsByTagName("input");

		for(var c=0;c < list.length;c++){
			if(list[c].name.substr(0,20) == "content_imgFileUpload"){
				content_imgFileUpload++;
			}
			content_imgFileUploadFor = true;
		}

	}

	content_imgFileUpload++;

	content_imgFileInnerHTML = "<input name='content_imgFileUpload-"+content_imgFileUpload+"' id='content_imgFileUpload-"+content_imgFileUpload+"' class='Upload' type='file'>";

	var content_imgDiv = document.getElementById("content_imgDiv");
	content_imgDiv.innerHTML = content_imgDiv.innerHTML + content_imgFileInnerHTML;

}

function restarContent_imgFileUpload (){


	if(!content_imgFileUploadFor){
		var list = document.getElementsByTagName("select");

		for(var c=0;c < list.length;c++){
			if(list[c].name.substr(0,21) == "content_imgFileUpload"){
				content_imgFileUpload++;
			}
			content_imgFileUploadFor = true;
		}
		content_imgFileUpload--;
	}

	if(content_imgFileUpload > '0'){
	var content_imgInnerHTML = document.getElementById("content_imgFileUpload-"+content_imgFileUpload);
	content_imgDiv.removeChild(content_imgInnerHTML);
	content_imgFileUpload--;
	}
}

function agregarButton_imgFile (){


	if(!button_imgFileFor){
		var list = document.getElementsByTagName("select");

		for(var c=0;c < list.length;c++){
			if(list[c].name.substr(0,11) == "content_img"){
				button_imgFile++;
			}
			button_imgFileFor = true;
		}
		button_imgFile--;
	}

	var button_imgFileInnerHTML = document.getElementById("button_imgFile-0").innerHTML;
	button_imgFile++;

	button_imgFileInnerHTML = '<select name="button_imgFile-'+button_imgFile+'"  id="button_imgFile-'+button_imgFile+'" style="width: 50%">'+button_imgFileInnerHTML+'</select>';

	var button_imgDiv = document.getElementById("button_imgDiv");
	button_imgDiv.innerHTML = button_imgDiv.innerHTML + button_imgFileInnerHTML;
}

function restarButton_imgFile (){

	if(!button_imgFileFor){
		var list = document.getElementsByTagName("select");

		for(var c=0;c < list.length;c++){
			if(list[c].name.substr(0,10) == "button_img"){
				button_imgFile++;
			}
			button_imgFileFor = true;
		}
		button_imgFile--;
	}

	if(button_imgFile > '0'){
		var button_imgFileInnerHTML = document.getElementById("button_imgFile-"+button_imgFile);
		button_imgDiv.removeChild(button_imgFileInnerHTML);
		button_imgFile--;
	}
}

function agregarButton_imgFileUpload (){

	button_imgFileUpload++;

	button_imgFileInnerHTML = "<input name='button_imgFileUpload-"+button_imgFileUpload+"' id='button_imgFileUpload-"+button_imgFileUpload+"' class='Upload' type='file'>";

	var button_imgDiv = document.getElementById("button_imgDiv");
	button_imgDiv.innerHTML = button_imgDiv.innerHTML + button_imgFileInnerHTML;

}

function restarButton_imgFileUpload (){
	if(button_imgFileUpload > '0'){
	var button_imgFileInnerHTML = document.getElementById("button_imgFileUpload-"+button_imgFileUpload);
	button_imgDiv.removeChild(button_imgFileInnerHTML);
	button_imgFileUpload--;
	}
}

function htmlDecode(input){
  input = input.replace(/&lt;/g, "<").replace(/&gt;/g, ">");
  return input;
}

function unescapeHTML(escapedHTML) {
  return escapedHTML.replace(/&lt;/g,'<').replace(/&gt;/g,'>').replace(/&amp;/g,'&');
}

function lista() {

	var lista = document.getElementById("lista");
	var formulario = document.getElementById("formulario");
	var campanasList = document.getElementById("campanasList");

	if(lista.style.display == 'inline'){
		lista.style.display = "none";
	}else{
		lista.style.display = "inline";
		formulario.style.display = "none";
		campanasList.style.display = "none";
	}

}

function formulario(){

	var formulario = document.getElementById("formulario");
	var lista = document.getElementById("lista");
	var campanasList = document.getElementById("campanasList");

	if(formulario.style.display == 'inline'){
		formulario.style.display = "none";
	}else{
		formulario.style.display = "inline";
		lista.style.display = "none";
		campanasList.style.display = "none";

	}

}

function campanas(){

	var campanasList = document.getElementById("campanasList");
	var formulario = document.getElementById("formulario");
	var lista = document.getElementById("lista");
	var formularioCampanaForm2 = document.getElementById("formularioCampanaForm2");

	if(campanasList.style.display == 'inline'){
		campanasList.style.display = "none";
				formularioCampanaForm2.style.display = "none";
	}else{
		campanasList.style.display = "inline";
		lista.style.display = "none";
		formulario.style.display = "none";
						formularioCampanaForm2.style.display = "none";
	}

}

function networksList(){

	var networksList = document.getElementById("networksList");

	if(networksList.style.display == 'inline'){
		networksList.style.display = "none";
	}else{
		networksList.style.display = "inline";
		formulario.style.display = "none";
		lista.style.display = "none";
	}

}

function editar(id){

	var consultaLanding = document.getElementById("consultaLanding");

	consultaLanding.value = id;
	document.getElementById("consultaLandingForm").submit();

}

function duplicar(id){

	var lista = document.getElementById("lista");
	lista.style.display = "none";

	var consultaLanding = document.getElementById("consultaLanding");

	consultaLanding.value = id;
	document.getElementById("consultaLandingForm").submit();

}

function clearForm(){

	var clear = document.getElementById("clear");
	clear.value = "";
	document.getElementById("clearForm").submit();

}

function productoUploadB () {

	var productoS = document.getElementById('productoS');
	var productoF = document.getElementById('productoF');

	productoF.disabled = false;
	productoS.disabled = true;

}

function productoSelectB () {

	var productoS = document.getElementById('productoS');
	var productoF = document.getElementById('productoF');

	productoF.disabled = true;
	productoS.disabled = false;

}

function background_imgS () {

	var background_imgS = document.getElementById('background_img-0');
	var background_imgF = document.getElementById('background_imgF');

	background_imgF.disabled = false;
	background_imgS.disabled = true;

}

function background_imgF () {

	var background_imgS = document.getElementById('background_img-0');
	var background_imgF = document.getElementById('background_imgF');

	background_imgF.disabled = true;
	background_imgS.disabled = false;

}

function cssSelectB () {

	var css_fileSelect = document.getElementById('css_fileSelect');
	var css_fileUpload = document.getElementById('css_fileUpload');

	css_fileSelect.disabled = false;
	css_fileUpload.disabled = true;

}

function cssUploadB () {

	var css_fileSelect = document.getElementById('css_fileSelect');
	var css_fileUpload = document.getElementById('css_fileUpload');

	css_fileSelect.disabled = true;
	css_fileUpload.disabled = false;

}

function logo_imgSelectB () {

	var logo_imgSelect = document.getElementById('logo_img-0');
	var logo_imgUpload = document.getElementById('logo_imgFile-0');

	logo_imgSelect.disabled = false;
	logo_imgUpload.disabled = true;

}

function logo_imgUploadB () {

	var logo_imgSelect = document.getElementById('logo_img-0');
	var logo_imgUpload = document.getElementById('logo_imgFile-0');

	logo_imgSelect.disabled = true;
	logo_imgUpload.disabled = false;

}

function layoutSelectB () {

	var layoutSelect = document.getElementById('layoutSelect');
	var layoutUpload = document.getElementById('layoutUpload');

	layoutSelect.disabled = false;
	layoutUpload.disabled = true;

}

function layoutUploadB () {

	var layoutSelect = document.getElementById('layoutSelect');
	var layoutUpload = document.getElementById('layoutUpload');

	layoutSelect.disabled = true;
	layoutUpload.disabled = false;

}

function crearCampana (){

	var str1 = "";
	var checkboxes = document.getElementsByName("check");
	var checkboxesChecked = [];

	if(checkboxes.length > 0){

		for (var i=0; i<checkboxes.length; i++) {
			if (checkboxes[i].checked) {
				checkboxesChecked.push(checkboxes[i]);
			}
		}

		for($d=0;$d<checkboxesChecked.length;$d++){

			if( $d == (checkboxesChecked.length - 1) ){
				var res = checkboxesChecked[$d].value;
				str1 = str1.concat(res);
			}else{
				var res = checkboxesChecked[$d].value+"_";
				str1 = str1.concat(res);
			}

		}

		var consultaCheckVar = document.getElementById("consultaCheck");
		consultaCheckVar.value = str1;
		document.getElementById("consultaCheckForm").submit();

	}

}

function editarCampana(id){

	var campanasList = document.getElementById("campanasList");
	campanasList.style.display = "none";

	var consultaCheck = document.getElementById("consultaCheck");
	var consultaCheckName = document.getElementById("consultaCheckName");

	consultaCheck.value = id;
	consultaCheckName.value = id;

	document.getElementById("consultaCheckForm").action = document.getElementById("consultaCheckForm").action + "?h=1";
	document.getElementById("consultaCheckForm").submit();

}

function campanaForm(){

	var formularioCampanaForm = document.getElementById("formularioCampanaForm");
	var formularioCampanaForm2 = document.getElementById("formularioCampanaForm2");

	if(formularioCampanaForm2.style.display == 'inline'){
		formularioCampanaForm2.style.display = "none";
	}else{
		formularioCampanaForm2.style.display = "inline";
	}
	if(formularioCampanaForm.style.display == 'inline'){
		formularioCampanaForm.style.display = "none";
	}else{
		formularioCampanaForm.style.display = "inline";
	}

}

function Bajar (value) {

	var bajarCampana = document.getElementById('bajarCampana');
	bajarCampana.value = value;
	document.getElementById("bajarCampanaForm").submit();

}

function secciones (){

	var seccionFormLanding = document.getElementById('consultaLanding');
	var seccionFormCheck = document.getElementById('consultaCheck');
	var consultaCheckName = document.getElementById('consultaCheckName');

	var parts = window.location.search.substr(1).split("&");
	var $_GET = {};

	for (var i = 0; i < parts.length; i++) {
	    var temp = parts[i].split("=");
	    $_GET[decodeURIComponent(temp[0])] = decodeURIComponent(temp[1]);
	}

	var parts = window.location.search.substr(1).split("&");
	var $_POST = {};
	for (var i = 0; i < parts.length; i++) {
	    var temp = parts[i].split("=");
	    $_POST[decodeURIComponent(temp[0])] = decodeURIComponent(temp[1]);
	}

	
	if(typeof($_GET['h']) != 'undefined'){

		var formCampana2 = document.getElementById("formCampana2");
		formCampana2.action = formCampana2.action + "?h=1";
		
	}

	if($_GET['landing_id'] > 0){

		var formulario = document.getElementById("formulario");
		var lista = document.getElementById("lista");
		var campanasList = document.getElementById("campanasList");

		if(formulario.style.display == "inline"){
			formulario.style.display = "inline";
			lista.style.display = "none";
			campanasList.style.display = "none";	
		}else{
			formulario.style.display = "none";
			lista.style.display = "inline";
			campanasList.style.display = "none";
		}

	} else if(seccionFormLanding != ""){

		var formulario = document.getElementById("formulario");
		var lista = document.getElementById("lista");
		var campanasList = document.getElementById("campanasList");
		var formularioCampanaForm2 = document.getElementById("formularioCampanaForm2");

		if(typeof($_GET['landing_id']) == 'undefined'){

			if(lista.style.display == 'inline'){
				lista.style.display = "none";
			}else{
				if($_GET['editar_landing_id'] > 0){
					formulario.style.display = "inline";
					lista.style.display = "none";
					campanasList.style.display = "none";//aca tendria que poner un flag
				}else{
					formulario.style.display = "none";
					lista.style.display = "inline";
					campanasList.style.display = "none";
				}

			}

		if(formularioCampanaForm2.style.display == 'inline'){
			formulario.style.display = "none";
			lista.style.display = "none";
			campanasList.style.display = "none";
		}else{
			formularioCampanaForm2.style.display = "inline";
			formulario.style.display = "none";
			lista.style.display = "none";
			campanasList.style.display = "none";
		}
		}else{

			if($_GET['landing_id']>0){
				formulario.style.display = "inline";
				lista.style.display = "none";
				campanasList.style.display = "none";
			}

		}

	} else if(seccionFormCheck != ""){

		var formulario = document.getElementById("formulario");
		var lista = document.getElementById("lista");
		var campanasList = document.getElementById("campanasList");
		//var formularioCampanaForm = document.getElementById("formularioCampanaForm");
		var formularioCampanaForm2 = document.getElementById("formularioCampanaForm2");

		if(formularioCampanaForm2.style.display == 'inline'){
			formulario.style.display = "none";
			lista.style.display = "none";
			campanasList.style.display = "none";
		}else{
			formularioCampanaForm2.style.display = "inline";
			formulario.style.display = "none";
			lista.style.display = "none";
			campanasList.style.display = "none";
		}

	}


}

</script>

</body>
</html>