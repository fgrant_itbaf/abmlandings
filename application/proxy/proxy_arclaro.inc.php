<?php

include __DIR__ . "/../functions_adnetworks.inc.php";
include __DIR__ . "/../functions_ssoclient.inc.php";

function proxy_arclaro(&$carrier_name, &$product_name, &$landing_name){

	global $logger;
	global $user_carrier_global;
	global $landing_name;
	global $cauto_conf;
	global $url_params;
	global $_SERVER;
	global $ani_global;
	global $myurl;
	global $ip_client;
	global $url_callback;

	if ($user_carrier_global != ""){
		$user_carrier_name = $user_carrier_global;

	}else {
		$user_carrier_name = get_carrier();
		$user_carrier_global = $user_carrier_name;
	
	}

	$logger->addInfo('Proxy-ClaroAr: detección de carrier', array('client_ip' => $ip_client, 'params' => array('carrier_name' => $user_carrier_name)));


	$lp_type  = "";
	$lp_name  = "";
	$lp_adnet = "";


	/* 	Tipos de landing page: por ej. auto
		Nombre de landing page
		Adnetwork: por ej. mobusi / adcolony
	*/

	if( strpos($landing_name, "_") !== false ){
		$landing_name_param = explode('_',$landing_name);

		$lp_type  =@ $landing_name_param[0];
		$lp_name  =@ $landing_name_param[1];
		$lp_adnet =@ $landing_name_param[2];

	}

	if($lp_type == ""){ $lp_type = "normal"; }

	$logger->addInfo('Proxy-ClaroAr: parámetros de landing page', array('client_ip' => $ip_client, 'params' => array('lp_type' => $lp_type, 'lp_name' => $lp_name, 'lp_adnet' => $lp_adnet)));


    $campaign = $lp_type . '_' . $lp_name . '_' . $lp_adnet;
    
	user_tracking('pg_landingv2_proxy_arclaro',
	    	$ip_client,
	    	$ani_global,
			@$_SERVER['HTTP_USER_AGENT'],
			$user_carrier_global,
			$myurl,
			@$_COOKIE['PHPSESSID'],
			@$_COOKIE['sso'],
			@$_COOKIE['_ga'], 
			$campaign,
			$_SERVER['SERVER_NAME'],
			$_SERVER['SERVER_ADDR']);


	//$smt_callback_url = "http://planeta.guru/sso/naranya/callback/" .$product_name. "/";
	//$smt_callback_url = urlencode($smt_callback_url);

	// Si es tráfico WAP redirige a SMT Argentina.
	if( $user_carrier_global == "ar_claro" ){

		## Consulta en la base la configuración a mostrar en el layout de la lp.
		$r = lp_config($carrier_name, $product_name, $landing_name);

		//$u = $cauto_conf["$user_carrier_global"]["$product_name"];
		$r['button_url'] = json_decode($r['button_url'], true);

		//$u = create_but_url($r["button_url"][0], null, $smt_callback_url);
		//$u = $r["button_url"][0];

		// Parse la url de botón
		$u = parse_url($r["button_url"][0]);

		// Convierte a array los parámetros de la url para pasarle luego al SSO
		parse_str($u["query"], $smt_url_params);



		######################################################################
		## ADCOLONY 															 #
		######################################################################

		if( $lp_type == "auto" && $lp_adnet == "adc" ){

			$map_carrier_data = array( 
				'ar_claro'	=> array(
								'saberfutbol' => array(
									'carrier_desc' => 'ArClaro-SaberFutbol',
							  		'channel' => 'adc_sf_cla_ar_' . $lp_name,
								  	'url_callback' => $url_callback["ar_claro"]["saberfutbol"]."/"
								),
								'juegos' => array(
									'carrier_desc' => 'ArClaro-Juegos',
							  		'channel' => 'adc_pgj_cla_ar_' . $lp_name,
								  	'url_callback' => $url_callback["ar_claro"]["juegos"]."/"
								)
				)
			);

			// Convierte en un array ($url_params) los params por url
			parse_str($_SERVER['QUERY_STRING'], $url_params);

			// Si es tráfico autorizado redirige.
			if(isset( $map_carrier_data["$user_carrier_name"]["$product_name"] ) && 
				$user_carrier_name == $carrier_name ){

				// Si existe channel redirige a url externa con callback
				if(isset($map_carrier_data["$carrier_name"]["$product_name"]["channel"])){

/*

					$callback_url = $map_carrier_data["$carrier_name"]["$product_name"]["url_callback"] .
							utm_adc_to_channel($url_params,  
												$map_carrier_data["$carrier_name"]["$product_name"]["channel"]);
					$callback_url = urlencode($callback_url);

					$logger->addInfo('Proxy-AdColony-ClaroAr: landing AdColony, genera url callback para redir', array('client_ip' => $ip_client, 'params' => array('callback_url' => urldecode($callback_url))));

					$u = $map_carrier_data["$carrier_name"]["$product_name"]["url"];
					$u = str_replace("##URLCALLBACK##", $callback_url, $u);
*/

					$channel = utm_adc_to_channel($url_params,  
									    			$map_carrier_data["$carrier_name"]["$product_name"]["channel"]);


					$logger->addInfo('Proxy-ClaroAr: SSO instancia.', array('developer' => 'itbaf', 'application' => $product_name, 'telco' => $carrier_name, 'channel' => $channel, 'params' => $smt_url_params));

					$url_sso = sso_init("itbaf",
										$product_name,
										$carrier_name,
										$channel,
										$smt_url_params);



				}else {
					$logger->addError('Proxy-AdColony-ArClaro: No se encuentra channel.', array('client_ip' => $ip_client, 'carrier_name' => $carrier_name, 
									'product_name' => $product_name));
				}

			}

		}else

		######################################################################
		## MOBUSI 															 #
		######################################################################

		if( $lp_type == "auto" && $lp_adnet == "mob" ){

			$map_carrier_data = array( 
				'ar_claro'	=> array(
					'saberfutbol' => array(
						'service_id' => '1957',
						'carrier_desc' => 'ArClaro-SaberFutbol',
				  		'channel' => 'mob_sf_cla_ar_' . $lp_name,
				  		'url' => $u,
					  	'url_callback' => $url_callback["ar_claro"]["saberfutbol"]."/"
					),
					'juegos' => array(
						'service_id' => '1957',
						'carrier_desc' => 'ArClaro-Juegos',
				  		'channel' => 'mob_pgj_cla_ar_' . $lp_name,
				  		'url' => $u,
					  	'url_callback' => $url_callback["ar_claro"]["juegos"]."/"
					)
				)
			);

			// Convierte en un array ($url_params) los params por url
			parse_str($_SERVER['QUERY_STRING'], $url_params);

			// Si es tráfico autorizado redirige.
			if(isset( $map_carrier_data["$user_carrier_name"]["$product_name"] ) && 
				$user_carrier_name == $carrier_name ){

				// Si existe channel redirige a url externa con callback
				if(isset($map_carrier_data["$carrier_name"]["$product_name"]["channel"])){

/*
					$callback_url = $map_carrier_data["$carrier_name"]["$product_name"]["url_callback"] .
							utm_mobusi_to_channel($url_params, 
												$map_carrier_data["$carrier_name"]["$product_name"]["service_id"], 
												$map_carrier_data["$carrier_name"]["$product_name"]["channel"]);
					$callback_url = urlencode($callback_url);

					$logger->addInfo('Proxy-Mobusi-ClaroAr: landing Mobusi, genera url callback para redir', array('client_ip' => $ip_client, 'params' => array('callback_url' => urldecode($callback_url))));

					$u = $map_carrier_data["$carrier_name"]["$product_name"]["url"];
					$u = str_replace("##URLCALLBACK##", $callback_url, $u);
*/

					$channel = utm_mobusi_to_channel($url_params,
													$map_carrier_data["$carrier_name"]["$product_name"]["service_id"],
									    			$map_carrier_data["$carrier_name"]["$product_name"]["channel"]);


					$logger->addInfo('Proxy-ClaroAr: SSO instancia.', array('developer' => 'itbaf', 'application' => $product_name, 'telco' => $carrier_name, 'channel' => $channel, 'params' => $smt_url_params));

					$url_sso = sso_init("itbaf",
										$product_name,
										$carrier_name,
										$channel,
										$smt_url_params);


				}else {
					$logger->addError('Proxy-Mobusi-ArClaro: No se encuentra channel.', array('client_ip' => $ip_client, 'carrier_name' => $carrier_name, 
									'product_name' => $product_name));
				}

			}
		
		######################################################################


		}else{

			$logger->addInfo('Proxy-ClaroAr: landing sin adnetwork, genera url de redir', array('client_ip' => $ip_client, 'params' => array('button_url' => $r["button_url"][0], 'callback_url' => $url_callback["ar_claro"]["$product_name"])));

			$u = create_but_url($r["button_url"][0], null, $url_callback["ar_claro"]["$product_name"]);

		}


	    $logger->addInfo('Proxy-ClaroAr: WAP redirige al SSO.', array('client_ip' => $ip_client, 'params' => $url_sso));
	
	    //var_dump($u); die();


	    header('Location: '. $url_sso );
		die();

	// Si es tráfico WEB muestra el layout (landing page) correspondiente.
	}else{

		// Si es tráfico de Adnetwork WEB
		if( $lp_type == "auto" && $lp_adnet != "" ){

			$logger->addError('Proxy-Adnetwork-ArClaro: Forbidden - Trafico no autorizado', array('client_ip' => $ip_client, 'params' => array('carrier_name' => $carrier_name, 'landing_name' => $landing_name)));

			header('HTTP/1.0 403 Forbidden');
			die();

		}

		/*
		switch ($product_name) {
		 	case 'juegos':
		 		$carrier_name = "ar_claro";
		 		$landing_name = "pg_claroarg";
		 		break;
		 	case 'saberfutbol':
		 		$carrier_name = "ar_claro";
		 		$landing_name = "sf2cla";
		 		break;
		 	case 'salute':
		 		$carrier_name = "all";
		 		$landing_name = "multi";
		 		break;
		 	case 'copasportia':
		 		$carrier_name = "all";
		 		$landing_name = "multi";
		 		break;
		 	
		 	default:
		 		$carrier_name = "all";
		 		$landing_name = "404";
		 		break;
		}
		*/


		$logger->addInfo('Proxy-ClaroAr: WEB muestra layout', array('client_ip' => $ip_client, 'params' => array('carrier_name' => $carrier_name, 'landing_name' => $landing_name)));

		//header('Location: '.$u );
		//exit();

	}
}
