<?php

function proxy_armovistar(&$carrier_name, &$product_name, &$landing_name){

	global $logger;
	global $user_carrier_global;
	global $landing_name;
	global $_SERVER;
	global $ani_global;
	global $urlbut;
	global $myurl;
	global $ip_client;


	if ($user_carrier_global != ""){
		$user_carrier_name = $user_carrier_global;

	}else {
		$user_carrier_name = get_carrier();
		$user_carrier_global = $user_carrier_name;
	
	}

	$logger->addInfo('Proxy-MovistarAr: detección de carrier', array('client_ip' => $ip_client, 'params' => array('user_carrier_name' => $user_carrier_name)));

	user_tracking('pg_landingv2_proxy_armovistar',
	    	$ip_client,
	    	$ani_global,
			@$_SERVER['HTTP_USER_AGENT'],
			$user_carrier_global,
			$myurl,
			@$_COOKIE['PHPSESSID'],
			@$_COOKIE['sso'],
			@$_COOKIE['_ga'], 
			get_campaign($myurl),
			$_SERVER['SERVER_NAME'],
			$_SERVER['SERVER_ADDR']);
	
	// Si es tráfico WAP redirige a SMS Consulting.
	if( $user_carrier_global == "ar_movistar" ){

		$u = $urlbut["ar_movistar"]["juegos"];

	    $logger->addInfo('Proxy-MovistarAr: WAP redirección a SMS-Consulting.', array('client_ip' => $ip_client, 'params' => $u));
	
	    header('Location: '.$u );
		exit();

	// Si es tráfico WEB muestra el layout (landing page) correspondiente.
	}else{

		switch ($product_name) {
		 	case 'juegos':
		 		$carrier_name = "ar_movistar";
		 		$landing_name = "mov";
		 		break;
		 	case 'saberfutbol':
		 		$carrier_name = "all";
		 		$landing_name = "multi";
		 		break;
		 	case 'salute':
		 		$carrier_name = "all";
		 		$landing_name = "multi";
		 		break;
		 	case 'copasportia':
		 		$carrier_name = "all";
		 		$landing_name = "multi";
		 		break;
		 	
		 	default:
		 		$carrier_name = "all";
		 		$landing_name = "404";
		 		break;
		 }
		 

		//$u = url_with_params("http://planeta.guru/juegos/sso/redir/?forceprovider=naranya", http_build_query($url_params));

		$logger->addInfo('Proxy-MovistarAr: WEB muestra layout', array('client_ip' => $ip_client, 'params' => array('carrier_name' => $carrier_name, 'landing_name' => $landing_name)));

		//header('Location: '.$u );
		//exit();

	}
}
