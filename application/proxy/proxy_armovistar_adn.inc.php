<?php

include __DIR__ . "/../functions_adnetworks.inc.php";
include __DIR__ . "/../functions_ssoclient.inc.php";

function proxy_armovistar_adn(&$carrier_name, &$product_name, &$landing_name){

	global $logger;
	global $user_carrier_global;
	global $landing_name;
	global $_SERVER;
	global $ani_global;
	global $urlbut;
	global $myurl;
	global $ip_client;
	global $url_callback;


	if ($user_carrier_global != ""){
		$user_carrier_name = $user_carrier_global;

	}else {
		$user_carrier_name = get_carrier();
		$user_carrier_global = $user_carrier_name;
	
	}


	$lp_type  = "";
	$lp_name  = "";
	$lp_adnet = "";


	/* 	Tipos de landing page: por ej. auto
		Nombre de landing page
		Adnetwork: por ej. mobusi / adcolony
	*/

	if( strpos($landing_name, "_") !== false ){
		$landing_name_param = explode('_',$landing_name);

		$lp_type  =@ $landing_name_param[0];
		$lp_name  =@ $landing_name_param[1];
		$lp_adnet =@ $landing_name_param[2];

	}

	if($lp_type == ""){ $lp_type = "normal"; }

	$logger->addInfo('Proxy-MovistarAr-Adn: parámetros de landing page', array('client_ip' => $ip_client, 'params' => array('lp_type' => $lp_type, 'lp_name' => $lp_name, 'lp_adnet' => $lp_adnet)));


	# Se hardcodea el user_carrier_global para forzar el tráfico a TIME
	//$user_carrier_global = 'ar_movistar';

    $campaign = $lp_type . '_' . $lp_name . '_' . $lp_adnet;

	user_tracking('pg_landingv2_proxy_armovistar_adn',
	    	$ip_client,
	    	$ani_global,
			@$_SERVER['HTTP_USER_AGENT'],
			$user_carrier_global,
			$myurl,
			@$_COOKIE['PHPSESSID'],
			@$_COOKIE['sso'],
			@$_COOKIE['_ga'], 
			$campaign,
			$_SERVER['SERVER_NAME'],
			$_SERVER['SERVER_ADDR']);


	// Si es tráfico WAP redirige a SMS Consulting.
	if( $user_carrier_global == "ar_movistar" ){

		//$u = $urlbut["ar_movistar"]["juegos"];

	    //$logger->addInfo('Proxy-MovistarAr: WAP redirección a SMS-Consulting.', array('client_ip' => $ip_client, 'params' => $u));
	
	    //header('Location: '.$u );
		//exit();

		## Consulta en la base la configuración a mostrar en el layout de la lp.
		$r = lp_config($carrier_name, $product_name, $landing_name);

		//$u = $cauto_conf["$user_carrier_global"]["$product_name"];
		$r['button_url'] = json_decode($r['button_url'], true);

		//$u = create_but_url($r["button_url"][0], null, $smt_callback_url);
		//$u = $r["button_url"][0];

		// Parse la url de botón
		//$u = parse_url($r["button_url"][0]);

		// Convierte a array los parámetros de la url para pasarle luego al SSO
		//parse_str($u["query"], $sso_url_params);


		######################################################################
		## ADCOLONY 															 #
		######################################################################

		if( $lp_type == "auto" && $lp_adnet == "adc" ){


			$map_carrier_data = array( 
				'ar_movistar'	=> array(
								'juegos' => array(
									'carrier_desc' => 'MovistarAr-Juegos',
							  		'channel' => 'adc_pgj_mov_ar_' . $lp_name,
							  		'url' => $r['button_url'][0],
								  	'url_callback' => $url_callback["ar_movistar"]["juegos"]."/"
								)
				)
			);


			// Convierte en un array ($url_params) los params por url
			parse_str($_SERVER['QUERY_STRING'], $url_params);

			// Si es tráfico autorizado redirige.
			if(isset( $map_carrier_data["$user_carrier_global"]["$product_name"] ) && 
				$user_carrier_global == $carrier_name ){

				// Si existe channel redirige a url externa con callback
				if(isset($map_carrier_data["$carrier_name"]["$product_name"]["channel"])){

/*
					$callback_url = $map_carrier_data["$carrier_name"]["$product_name"]["url_callback"] .
							utm_adc_to_channel($url_params,  
												$map_carrier_data["$carrier_name"]["$product_name"]["channel"]);

					//$callback_url = urlencode($callback_url);

					// Se le pasa al SSO (en la url de callback el ani)
					$callback_url = $callback_url ."?ani=" .base64_encode($ani_global);

					$callback_url_b64 = base64_encode($callback_url);

					$logger->addInfo('Proxy-AdColony-MovistarAr: landing AdColony, genera url callback para redir', array('client_ip' => $ip_client, 'params' => array('callback_url' => $callback_url, 'callback_url_b64' => $callback_url_b64)));

					$u = $map_carrier_data["$carrier_name"]["$product_name"]["url"];
					$u = str_replace("##URLCALLBACK##", $callback_url_b64, $u);
*/

					$channel = utm_adc_to_channel($url_params,  
									    			$map_carrier_data["$carrier_name"]["$product_name"]["channel"]);


					$sso_params = array("uri" => $map_carrier_data["$carrier_name"]["$product_name"]["url"],
										"ani" => $ani_global);

					$logger->addInfo('Proxy-MovistarAr: SSO instancia.', array('developer' => 'itbaf', 'application' => $product_name, 'telco' => $carrier_name, 'channel' => $channel, 'params' => $sso_params));

					$url_sso = sso_init("itbaf",
										$product_name,
										$carrier_name,
										$channel,
										$sso_params);

					//echo $url_sso; die();

				}else {
					$logger->addError('Proxy-AdColony-MovistarAr: No se encuentra channel.', array('client_ip' => $ip_client, 'carrier_name' => $carrier_name, 
									'product_name' => $product_name));
				}

			}

		}else

		######################################################################
		## MOBUSI 															 #
		######################################################################

		if( $lp_type == "auto" && $lp_adnet == "mob" ){


			$map_carrier_data = array( 
				'ar_movistar'	=> array(
								'juegos' => array(
									'service_id' => '1957',
									'carrier_desc' => 'MovistarAr-Juegos',
							  		'channel' => 'mob_pgj_mov_ar_' . $lp_name,
							  		'url' => $r['button_url'][0],
								  	'url_callback' => $url_callback["ar_movistar"]["juegos"]."/"
								)
				)
			);



			// Convierte en un array ($url_params) los params por url
			parse_str($_SERVER['QUERY_STRING'], $url_params);

			// Si es tráfico autorizado redirige.
			if(isset( $map_carrier_data["$user_carrier_global"]["$product_name"] ) && 
				$user_carrier_global == $carrier_name ){

				// Si existe channel redirige a url externa con callback
				if(isset($map_carrier_data["$carrier_name"]["$product_name"]["channel"])){


/*

					$callback_url = $map_carrier_data["$carrier_name"]["$product_name"]["url_callback"] .
							utm_mobusi_to_channel($url_params, 
												$map_carrier_data["$carrier_name"]["$product_name"]["service_id"], 
												$map_carrier_data["$carrier_name"]["$product_name"]["channel"]);
					//$callback_url = urlencode($callback_url);
					
					// Se le pasa al SSO (en la url de callback el ani)
					$callback_url = $callback_url ."?ani=" .base64_encode($ani_global);

					$callback_url_b64 = base64_encode($callback_url);

					$logger->addInfo('Proxy-Mobusi-MovistarAr: landing Mobusi, genera url callback para redir', array('client_ip' => $ip_client, 'params' => array('callback_url' => $callback_url, 'callback_url_b64' => $callback_url_b64)));

					$u = $map_carrier_data["$carrier_name"]["$product_name"]["url"];
					$u = str_replace("##URLCALLBACK##", $callback_url_b64, $u);

*/

					$channel = utm_mobusi_to_channel($url_params, 
												$map_carrier_data["$carrier_name"]["$product_name"]["service_id"], 
												$map_carrier_data["$carrier_name"]["$product_name"]["channel"]);


					$sso_params = array("uri" => $map_carrier_data["$carrier_name"]["$product_name"]["url"],
										"ani" => $ani_global);

					$logger->addInfo('Proxy-MovistarAr: SSO instancia.', array('developer' => 'itbaf', 'application' => $product_name, 'telco' => $carrier_name, 'channel' => $channel, 'params' => $sso_params));

					$url_sso = sso_init("itbaf",
										$product_name,
										$carrier_name,
										$channel,
										$sso_params);


				}else {
					$logger->addError('Proxy-Mobusi-MovistarAr: No se encuentra channel.', array('client_ip' => $ip_client, 'carrier_name' => $carrier_name, 
									'product_name' => $product_name));
				}

			}
		}else{

			$logger->addError('Proxy-MovistarAr: landing sin adnetwork, genera url de redir', array('client_ip' => $ip_client, 'params' => array('button_url' => $r["button_url"][0], 'callback_url' => $url_callback["ar_claro"]["$product_name"])));

			header('HTTP/1.0 500 Internal Server Error');
			die();

		}

		//$u = $u. '&ani=' .$ani_global;

		//$logger->addInfo('Proxy-MovistarAr-Adn: WAP redirección a SMS-Consulting.', array('client_ip' => $ip_client, 'params' => $u));
	
	    //var_dump($u); die();

	    //header('Location: '.$u);
		
		$logger->addInfo('Proxy-MovistarAr-Adn: WAP redirige al SSO.', array('client_ip' => $ip_client, 'params' => $url_sso));
	
	    //var_dump($u); die();


	    header('Location: '. $url_sso );
		die();
		
		######################################################################


	// Si es tráfico WEB muestra error 403 Forbidden.
	}else{

		$logger->addError('Proxy-Adnetwork-MovistarAr: Forbidden - Trafico no autorizado', array('client_ip' => $ip_client, 'params' => array('carrier_name' => $carrier_name, 'landing_name' => $landing_name)));

		header('HTTP/1.0 403 Forbidden');
		die();

	}
}
