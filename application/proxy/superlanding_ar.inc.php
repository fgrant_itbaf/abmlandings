<?php

function superlanding_ar(&$carrier_name, &$product_name, &$landing_name){

	global $logger;
	global $user_carrier_global;
	global $cauto_conf;
	global $url_params;
	global $_SERVER;
	global $ani_global;
	global $url_callback;
	global $myurl;
	global $ip_client;


	if ($user_carrier_global != ""){
		$user_carrier_name = $user_carrier_global;

	}else {
		$user_carrier_name = get_carrier();
		$user_carrier_global = $user_carrier_name;
	
	}

	$logger->addInfo('Superlanding: detección de carrier', array('client_ip' => $ip_client, 'params' => array('user_carrier_name' => $user_carrier_name)));

	
	if(isset($cauto_conf["$user_carrier_name"]["$product_name"])){

		## En el caso que sea AR_CLARO, se setea la url de callback (url encodeada) con los utms recibidos.
		if( $user_carrier_name == 'ar_claro' ){

			$smt_callback_url = $url_callback["ar_claro"]["$product_name"]. 
								"/?channel=" . utm_to_channel($url_params);

			$smt_callback_url = urlencode($smt_callback_url);

			$u = $cauto_conf["$user_carrier_name"]["$product_name"];

	    	$u = str_replace("##URLCALLBACK##", $smt_callback_url, $u);
		
		}else {
			$u = url_with_params($cauto_conf["$user_carrier_name"]["$product_name"], http_build_query($url_params));

		}

	    

	    ###############################################################

	    $logger->addInfo('Superlanding: redirección', array('client_ip' => $ip_client, 'params' => $u));

	    user_tracking('pg_landingv2_superlanding',
	    	$ip_client,
	    	$ani_global,
			@$_SERVER['HTTP_USER_AGENT'],
			$user_carrier_global,
			$myurl,
			@$_COOKIE['PHPSESSID'],
			@$_COOKIE['sso'],
			@$_COOKIE['_ga'], 
			get_campaign($myurl),
			$_SERVER['SERVER_NAME'],
			$_SERVER['SERVER_ADDR']);

	    header('Location: '.$u );
		exit();
	
	}else {
	    $carrier_name = "all";
		$landing_name = "multi";
	
		$logger->addInfo('Superlanding: muestra multi-carrier', array('client_ip' => $ip_client, 'params' => array('carrier_name' => $carrier_name, 'landing_name' => $landing_name)));

	}
}
