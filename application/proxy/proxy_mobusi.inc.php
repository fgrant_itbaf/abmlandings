<?php

include __DIR__ . "/../functions_adnetworks.inc.php";

function proxy_mobusi(&$carrier_name, &$product_name, &$landing_name){

	global $logger;
	global $user_carrier_global;
	global $landing_name;
	global $urlbut;
	#global $url_params;
	global $_SERVER;
	global $ani_global;
	global $myurl;
	global $ip_client;


	$map_carrier_data = array( 

					'ar_claro' 	  => array(
									'saberfutbol' => array(
										'service_id' => '1957',
										'carrier_desc' => 'ArClaro-SaberFutbol',
								  		'channel' => 'mob_sf_cla_ar_tevez',
								  		'url' => 'http://smtservicios.com.ar/portalone/subscribe.action?pid=MDSP2000016561&sid=0020572000015371&access=wap&language=es&pic=http://planeta.guru/land/images/smt/ar/saberfutbol/fondo-futbol.gif&buttonPic=http://planeta.guru/land/images/smt/ar/saberfutbol/boton.gif&url=##URLCALLBACK##',
								  		'url_callback' => 'http://planeta.guru/sso/naranya/callback/saberfutbol/' )
									),

				   'ar_movistar' => array(
				   					'juegos' => array(
				   						'service_id' => '1957',
					   					'carrier_desc' => 'ArMovistar-Juegos',
					   					'utms' => array( 
		   									'utm_source' => 'mob',
		   									'utm_medium' => 'pgj_mov_arg',
		   									'utm_campaign' => 'navidad'),
					   			  		'url' => $urlbut['ar_movistar_ssocallback']['juegos'])
				   					),

				   'co_claro' 	  => array( 
				   					'juegos' => array( 
					   					'service_id' => '1957',
					   					'carrier_desc' => 'CoClaro-Juegos',
					   					'channel' => 'mob_pgj_cla_co_juegos',
					   			  		'url' => $urlbut["co_claro"]["juegos"],
					   			  		'url_callback' => 'http://rxr.co/cbcoclaropgj/')
				   					)
				   );


	if ($user_carrier_global != ""){
		$user_carrier_name = $user_carrier_global;

	}else {
		$user_carrier_name = get_carrier();
		$user_carrier_global = $user_carrier_name;
	
	}

	// TESTING AR_MOVISTAR
	//$user_carrier_name = 'ar_movistar';

	$carrier_desc = $map_carrier_data["$carrier_name"]["$product_name"]["carrier_desc"];


	$logger->addInfo('Proxy-Mobusi-' .$carrier_desc. ': detección de carrier', array('client_ip' => $ip_client, 'params' => array('carrier_name' => $user_carrier_name)));

    $campaign = $map_carrier_data["$carrier_name"]["$product_name"]["channel"];

	user_tracking('pg_landingv2_proxymobusi',
	    	$ip_client,
	    	$ani_global,
			@$_SERVER['HTTP_USER_AGENT'],
			$user_carrier_global,
			$myurl,
			@$_COOKIE['PHPSESSID'],
			@$_COOKIE['sso'],
			@$_COOKIE['_ga'], 
			$campaign,
			$_SERVER['SERVER_NAME'],
			$_SERVER['SERVER_ADDR']);
	

	// Convierte en un array ($url_params) los params por url
	parse_str($_SERVER['QUERY_STRING'], $url_params);

	// Si es tráfico autorizado redirige.

	if(isset( $map_carrier_data["$user_carrier_name"]["$product_name"] ) && 
				$user_carrier_name == $carrier_name ){

		#var_dump($url_params); die;
		#var_dump(utm_mobusi_to_channel($url_params)); die;


		// Si existe channel redirige a url externa con callback
		if(isset($map_carrier_data["$carrier_name"]["$product_name"]["channel"])){

			///////// Hack para Claro-Co (Americamovil) /////////
			$sep = '';

			if($carrier_name == 'co_claro'){
				$sep = '?channel=';

			}
			/////////////////////////////////////////////////////

			$callback_url = $map_carrier_data["$carrier_name"]["$product_name"]["url_callback"] . $sep . 
					utm_mobusi_to_channel($url_params, 
										$map_carrier_data["$carrier_name"]["$product_name"]["service_id"], 
										$map_carrier_data["$carrier_name"]["$product_name"]["channel"]);
			$callback_url = urlencode($callback_url);

			$u = $map_carrier_data["$carrier_name"]["$product_name"]["url"];
			$u = str_replace("##URLCALLBACK##", $callback_url, $u);


		}else {

			// Si la url tiene placeholder de urlcallback (hack p/ sso Claro-Co)
			if(strpos($map_carrier_data["$carrier_name"]["$product_name"]["url"], '##URLCALLBACK##') !== false ){

				$callback_url = mobusi_url_utms($url_params, 
						$map_carrier_data["$carrier_name"]["$product_name"]["service_id"],
						$map_carrier_data["$carrier_name"]["$product_name"]["utms"], 
						$map_carrier_data["$carrier_name"]["$product_name"]["url_callback"]);

				$callback_url = urlencode($callback_url);


				$u = str_replace('##URLCALLBACK##', 
							$callback_url, 
							$map_carrier_data["$carrier_name"]["$product_name"]["url"]);


			}else {

				$u = mobusi_url_utms($url_params,
						$map_carrier_data["$carrier_name"]["$product_name"]["service_id"],
						$map_carrier_data["$carrier_name"]["$product_name"]["utms"], 
						$map_carrier_data["$carrier_name"]["$product_name"]["url"]);

			}
			

		}

		//var_dump(get_defined_vars()); die;

	    $logger->addInfo('Proxy-Mobusi-' .$carrier_desc. ': WAP redirección.', array('client_ip' => $ip_client, 'params' => $u));
	
	    header('Location: '.$u );
		exit();

	// Si es tráfico no autorizado muestra error.
	}else{

		$logger->addError('Proxy-Mobusi-' .$carrier_desc. ': Forbidden - Trafico no autorizado', array('client_ip' => $ip_client, 'params' => array('carrier_name' => $carrier_name, 'landing_name' => $landing_name)));

		header('HTTP/1.0 403 Forbidden');
		die();


	}
}
