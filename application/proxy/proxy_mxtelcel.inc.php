<?php

function proxy_mxtelcel(&$carrier_name, &$product_name, &$landing_name){

	global $logger;
	global $user_carrier_global;
	global $landing_name;
	global $_SERVER;
	global $ani_global;
	global $urlbut;
	global $myurl;
	global $ip_client;


	if ($user_carrier_global != ""){
		$user_carrier_name = $user_carrier_global;

	}else {
		$user_carrier_name = get_carrier();
		$user_carrier_global = $user_carrier_name;
	
	}

	$logger->addInfo('Proxy-TelcelMx: detección de carrier', array('client_ip' => $ip_client, 'params' => array('user_carrier_name' => $user_carrier_name)));

	user_tracking('pg_landingv2_proxy_mxtelcel',
	    	$ip_client,
	    	$ani_global,
			@$_SERVER['HTTP_USER_AGENT'],
			$user_carrier_global,
			$myurl,
			@$_COOKIE['PHPSESSID'],
			@$_COOKIE['sso'],
			@$_COOKIE['_ga'], 
			get_campaign($myurl),
			$_SERVER['SERVER_NAME'],
			$_SERVER['SERVER_ADDR']);
	
	


	// Redir al SSO
	$u = $urlbut["mx_claro"]["juegos"];
	$u = (strpos($u, '?') !== false) ? $u . "&ani=" . $ani_global : $u . "?ani=" . $ani_global;

    $logger->addInfo('Proxy-TelcelMx: Instancia el SSO y redirige.', array('client_ip' => $ip_client, 'params' => $u, 'ani' => $ani_global, 'channel' => $channel));

    header('Location: '.$u );
	exit();

}
