<?php

function proxy_coclaro(&$carrier_name, &$product_name, &$landing_name){

	global $logger;
	global $user_carrier_global;
	global $cauto_conf;
	global $url_params;
	global $_SERVER;
	global $ani_global;
	global $myurl;
	global $url_callback;
	global $param_utm_source;
	global $ip_client;


	$logger->addInfo('Superlanding-Co: detección de carrier', 
		array('client_ip' => $ip_client, 'params' => array('carrier_name' => $carrier_name)));


	if ($user_carrier_global != ""){
		$user_carrier_name = $user_carrier_global;

	}else {
		$user_carrier_name = get_carrier();
		$user_carrier_global = $user_carrier_name;
	
	}

	user_tracking('pg_landingv2_superlandingco',
	    	$ip_client,
	    	$ani_global,
			@$_SERVER['HTTP_USER_AGENT'],
			$user_carrier_global,
			$myurl,
			@$_COOKIE['PHPSESSID'],
			@$_COOKIE['sso'],
			@$_COOKIE['_ga'], 
			get_campaign($myurl),
			$_SERVER['SERVER_NAME'],
			$_SERVER['SERVER_ADDR']);
	
	//if(isset($cauto_conf["$carrier_name"]["$product_name"])){
	
	$smt_callback_url = $url_callback["co_claro"]["$product_name"]. "/?channel=" . utm_to_channel($url_params);
	$smt_callback_url = urlencode($smt_callback_url);

	# Si viene por WAP o WEB/Facebook, lo envía a SMT Co, sino al SSO.
	if( $user_carrier_global == "co_claro" || $param_utm_source == 'facebook' ){
	    // $u = url_with_params('http://smtservicios.com/portalone/subscribe.action?&pid=MDSP2000017421&sid=0021352000016210&access=wap&adprovider=Telcel&pic=http://planeta.guru/juegos/landing/co/claro/personaljuegos.png&language=es&css=http://planeta.guru/juegos/landing/co/claro/po_claro_co.css&url=http://bit.ly/29JC53I', http_build_query($url_params));
	    
		if( isset($cauto_conf["co_claro"]["$product_name"]) ){

			$u = $cauto_conf["co_claro"]["$product_name"];
	    	$u = str_replace("##URLCALLBACK##", $smt_callback_url, $u);

		    $logger->addInfo('Superlanding-Co: trafico WAP o WEB desde Facebook, redireccion a SMT.', 
		    	array('client_ip' => $ip_client, 'params' => $u));
		
		    header('Location: '.$u );
			exit();

		}else {

			$logger->addWarning('Superlanding-Co: trafico WAP. No se encuentra url de SMT.', 
				array('client_ip' => $ip_client, 
					'params' => array('carrier' => 'autoco', 
						'product' => $product_name, 
						'landing' => 'auto')));

			header( 'HTTP/1.0 404 Not Found' );
			exit();

		}

		

	// Si es tráfico WEB muestra el layout (landing page) correspondiente.
	}else{

		switch ($product_name) {
		 	case 'juegos':
		 		$u = url_with_params("http://planeta.guru/juegos/sso/redir/?forceprovider=AmericaMovil", http_build_query($url_params));
		 		break;
		 	case 'saberfutbol':
		 		$u = "";
		 		break;
		 	case 'salute':
		 		$u = "";
		 		break;
		 	case 'copasportia':
		 		$u = "";
		 		break;
		 	
		 	default:
		 		$u = "";
		 		break;
		 }


		if($u != ""){
			$logger->addInfo('Superlanding-Co: trafico WEB. Redirección al SSO.', 
				array('client_ip' => $ip_client, 
					'params' => array('url' => $u)));

			$redir = 'Location: '.$u;

		}else {
			$logger->addWarning('Superlanding-Co: trafico WEB. No se encuentra url de redireccion.', 
				array('client_ip' => $ip_client, 
					'params' => array('carrier' => 'autoco', 
						'product' => $product_name, 
						'landing' => 'auto')));

			$redir = 'HTTP/1.0 404 Not Found';

		}

		header($redir);
		exit();

	}
}
