<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function __construct () {

		parent::__construct();

		$this->output->delete_cache();
		$this->load->model('index', '');
		$this->load->library('table');
		$this->load->library('javascript');
		$this->load->helper('url');

		$this->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . ' GMT');('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
		$this->output->set_header('Pragma: no-cache');
		$this->output->set_header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");

	}

	public function index()
	{
		$this->load->view('index');
		$this->load->helper('url');
	}

}